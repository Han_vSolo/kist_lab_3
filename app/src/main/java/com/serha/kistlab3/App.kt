package com.serha.kistlab3

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import android.net.ConnectivityManager
import com.serha.kistlab3.db.AppDatabase
import com.serha.kistlab3.di.appModule
import org.koin.android.ext.android.startKoin
import java.util.*

class App : Application() {

    lateinit var database: AppDatabase
    var listOfStrings = ArrayList<String>()

    companion object {
        lateinit var mInstance: App
            private set
    }

    override fun onCreate() {
        super.onCreate()
        mInstance = this
        database =
                Room.databaseBuilder(this, AppDatabase::class.java, "database")
                    .build()
        startKoin(this, listOf(appModule))
    }

    fun isOnline(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnected
    }

    fun getArray(): Array<String?> {
        var array = arrayOfNulls<String>(listOfStrings.size)

        for (i in listOfStrings.indices) {
            array[i] = listOfStrings[i]
        }

//        Arrays.sort(array)
        return array
    }
}