package com.serha.kistlab3

import com.serha.kistlab3.db.entities.FullPerson

class FullPersonModel {

    var person_id: Long = 0

    var surname: String = ""

    var name: String = ""

    var patronymic: String = ""

    var sex: String = ""

    var birth_date: String = ""

    var birth_place: String = ""

    var address: String = ""

    var telephon: String = ""

    var finance_name: String = ""

    var social_name: String = ""

    var privilege_name: String = ""

    var priv_begin_date: String = ""

    var priv_end_date: String = ""

    var diploma_name: String = ""

    var group_code: String = ""

    var putting_date: String = ""

    var speciality_name: String = ""

    var cafedra_name: String = ""

    var contract_kind_name: String = ""

    var contract_date: String = ""

    var payment_sum: ArrayList<Long> = ArrayList<Long>()

    var payment_date: ArrayList<String> = ArrayList<String>()

    var payment_id: ArrayList<Long> = ArrayList<Long>()

    var mark_name: ArrayList<String> = ArrayList<String>()

    var markId: ArrayList<Long> = ArrayList<Long>()

    fun insertPerson(person: FullPerson) {
        this.person_id = person.person_id
        this.surname = person.surname
        this.name = person.name
        this.patronymic = person.patronymic
        this.sex = person.sex
        this.birth_date = person.birth_date
        this.birth_place = person.birth_place
        this.address = person.address
        this.telephon = person.telephon
        this.finance_name = person.finance_name
        this.social_name = person.social_name
        this.privilege_name = person.privilege_name
        this.priv_begin_date = person.priv_begin_date
        this.priv_end_date = person.priv_end_date
        this.diploma_name = person.diploma_name
        this.group_code = person.group_code
        this.putting_date = person.putting_date
        this.speciality_name = person.speciality_name
        this.cafedra_name = person.cafedra_name
        this.contract_kind_name = person.contract_kind_name
        this.contract_date = person.contract_date
        this.payment_sum.add(person.payment_sum)
        this.payment_date.add(person.payment_date)
        this.payment_id.add(person.payment_id)
        this.mark_name.add(person.mark_name)
        this.markId.add(person.markId)
    }
}