package com.serha.kistlab3.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.serha.kistlab3.FullPersonModel
import com.serha.kistlab3.R
import com.serha.kistlab3.mvp.base.BaseActivity
import kotlinx.android.synthetic.main.item_person.view.*

class ContentAdapter(val contents: ArrayList<FullPersonModel>, private val activity: BaseActivity?, val type: Int) :
    RecyclerView.Adapter<ContentAdapter.ContentViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentAdapter.ContentViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_person, parent, false)
        val viewHolder = ContentViewHolder(itemView)
        return viewHolder
    }

    override fun onBindViewHolder(holder: ContentAdapter.ContentViewHolder, position: Int) {
        val content = contents[position]
        when (type) {
            3 -> {
                holder.personTable.visibility = View.GONE
                holder.markTable.visibility = View.GONE
                holder.groupTable.visibility = View.VISIBLE

                holder.groupTextView.text = content.group_code
            }
            4 -> {
                holder.personTable.visibility = View.GONE
                holder.markTable.visibility = View.VISIBLE
                holder.groupTable.visibility = View.GONE

                holder.markTextView.text = content.mark_name[0]
            }
            else -> {
                holder.personTable.visibility = View.VISIBLE
                holder.markTable.visibility = View.GONE
                holder.groupTable.visibility = View.GONE

                holder.fio.text = content.surname + " " + content.name + " " +
                        content.patronymic
                holder.sex.text = content.sex
                holder.birth.text = content.birth_date + ", " + content.birth_place
                holder.address.text = content.address
                holder.telephon.text = content.telephon
                holder.finance.text = content.finance_name
                holder.social.text = content.social_name
                holder.privilege.text = content.privilege_name
                holder.privilegeLong.text = content.priv_begin_date + " - " + content.priv_end_date
                holder.diploma.text = content.diploma_name
                holder.group.text = content.group_code + " (" + content.putting_date + ")"
                holder.speciality.text = content.speciality_name
                holder.cafedra.text = content.cafedra_name
                holder.contract.text = content.contract_kind_name + " (" + content.contract_date + ")"

                var markString = ""
                for (i in content.markId.indices) {
                    if (i != 0) {
                        markString += ", "
                    }
                    markString += content.mark_name[i]
                }
                holder.mark.text = markString

                var paymentString = ""
                for (i in content.payment_id.indices) {
                    if (i != 0) {
                        paymentString += ", "
                    }
                    paymentString += content.payment_sum[i].toString() + " (" + content.payment_date[i] + ")"
                }
                holder.payment.text = paymentString
            }
        }
    }

    override fun getItemCount(): Int {
        return contents.size
    }


    inner class ContentViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var fio = itemView.fio
        var sex = itemView.sex
        var birth = itemView.birth
        var address = itemView.address
        var telephon = itemView.telephon
        var finance = itemView.finance
        var social = itemView.social
        var privilege = itemView.privilege
        var privilegeLong = itemView.privilegeLong
        var diploma = itemView.diploma
        var group = itemView.group
        var speciality = itemView.speciality
        var cafedra = itemView.cafedra
        var contract = itemView.contract
        var payment = itemView.payment
        var mark = itemView.mark

        var markTable = itemView.marksTable
        var personTable = itemView.personsTable
        var groupTable = itemView.groupsTable

        var markTextView = itemView.markTextView
        var groupTextView = itemView.groupTextView
    }
}