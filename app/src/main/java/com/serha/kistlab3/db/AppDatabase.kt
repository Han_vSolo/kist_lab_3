package com.serha.kistlab3.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.serha.kistlab3.db.daos.*
import com.serha.kistlab3.db.entities.*

@Database(
    entities = [Cafedra::class, Contract::class, Groups::class, Payment::class,
        Person::class, PersonPrivilege::class, SContractKind::class, SDiploma::class,
        SFinance::class, SMark::class, Speciality::class, SPrivilege::class,
        SSocial::class, StudentMarks::class, Student::class, StudentGroup::class], version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun cafedraDao(): CafedraDao

    abstract fun contractDao(): ContractDao

    abstract fun groupsDao(): GroupsDao

    abstract fun paymentDao(): PaymentDao

    abstract fun personDao(): PersonDao

    abstract fun personPrivilegeDao(): PersonPrivilegeDao

    abstract fun sContractKindDao(): SContractKindDao

    abstract fun sDiplomaDao(): SDiplomaDao

    abstract fun sFinanceDao(): SFinanceDao

    abstract fun sMarkDao(): SMarkDao

    abstract fun specialityDao(): SpecialityDao

    abstract fun sPrivilegeDao(): SPrivilegeDao

    abstract fun sSocialDao(): SSocialDao

    abstract fun studentDao(): StudentDao

    abstract fun studentGroupDao(): StudentGroupDao

    abstract fun studentMarksDao(): StudentMarksDao
}