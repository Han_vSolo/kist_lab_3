package com.serha.kistlab3.db.daos

import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Transaction

interface BaseDao<T> {
    @Insert
    fun insert(vararg obj: T)

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(obj: List<T>)
}