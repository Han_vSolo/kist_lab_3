package com.serha.kistlab3.db.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.serha.kistlab3.db.entities.Cafedra
import io.reactivex.Maybe

@Dao
abstract class CafedraDao : BaseDao<Cafedra> {

    @Query(
        "select \n" +
                "  *\n" +
                "from s_cafedra"
    )
    abstract fun getAllCafedras(): Maybe<List<Cafedra>>
}