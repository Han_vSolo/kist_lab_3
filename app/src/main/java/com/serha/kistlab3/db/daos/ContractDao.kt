package com.serha.kistlab3.db.daos

import android.arch.persistence.room.Dao
import com.serha.kistlab3.db.entities.Contract

@Dao
abstract class ContractDao : BaseDao<Contract> {

}