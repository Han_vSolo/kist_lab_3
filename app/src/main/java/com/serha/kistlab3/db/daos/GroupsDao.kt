package com.serha.kistlab3.db.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.serha.kistlab3.db.entities.FullGroup
import com.serha.kistlab3.db.entities.Groups
import io.reactivex.Maybe

@Dao
abstract class GroupsDao : BaseDao<Groups> {

    @Query(
        "select \n" +
                "  g.group_code,\n" +
                "  g.group_creat_date\n" +
                "from s_cafedra as sc\n" +
                "left join speciality as sp on sp.cafedra_id = sc.cafedra_id\n" +
                "left join groups as g on g.speciality_id = sp.speciality_id\n" +
                "where sc.cafedra_name = :string\n"
    )
    abstract fun getGroupsByCafedra(string: String): Maybe<List<FullGroup>>
}