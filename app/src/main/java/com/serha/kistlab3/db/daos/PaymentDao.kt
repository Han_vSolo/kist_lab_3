package com.serha.kistlab3.db.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.serha.kistlab3.db.entities.Payment
import io.reactivex.Maybe

@Dao
abstract class PaymentDao : BaseDao<Payment> {

    @Query(
        "select \n" +
                "  *\n" +
                "from payment"
    )
    abstract fun getAllPayments(): Maybe<List<Payment>>
}