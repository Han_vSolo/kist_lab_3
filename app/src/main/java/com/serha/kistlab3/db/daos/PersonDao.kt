package com.serha.kistlab3.db.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.serha.kistlab3.db.entities.FullPerson
import com.serha.kistlab3.db.entities.Person
import io.reactivex.Maybe

@Dao
abstract class PersonDao : BaseDao<Person> {

    @Query(
        "select \n" +
                "  p.person_id, \n" +
                "  p.surname, \n" +
                "  p.name, \n" +
                "  p.patronymic,\n" +
                "  p.sex,\n" +
                "  p.birth_date,\n" +
                "  p.birth_place,\n" +
                "  p.address,\n" +
                "  p.telephon,\n" +
                "  f.finance_name,\n" +
                "  ss.social_name,\n" +
                "  sp.privilege_name,\n" +
                "  pp.priv_begin_date,\n" +
                "  pp.priv_end_date,\n" +
                "  sd.diploma_name,\n" +
                "  sd.diploma_name,\n" +
                "  g.group_code,\n" +
                "  sg.putting_date,\n" +
                "  spe.speciality_name,\n" +
                "  sc.cafedra_name,\n" +
                "  sck.contract_kind_name,\n" +
                "  c.contract_date,\n" +
                "  pay.payment_date,\n" +
                "  pay.payment_sum,\n" +
                "  pay.payment_id,\n" +
                "  sma.mark_name,\n" +
                "  sm.id\n" +
                "from person as p\n" +
                "left join student as s on p.person_id = s.person_id \n" +
                "left join s_finance as f on s.finance_id = f.finance_id\n" +
                "left join s_social as ss on ss.social_id = p.social_id\n" +
                "left join person_privilege as pp on p.person_id = pp.person_id\n" +
                "left join s_privilege as sp on pp.privilege_id = sp.privilege_id\n" +
                "left join s_diploma as sd on s.diploma_id = sd.diploma_id\n" +
                "left join contract as c on c.student_id = s.student_id\n" +
                "left join s_contract_kind as sck on c.contract_kind_id = sck.contract_kind_id\n" +
                "left join student_group as sg on s.student_id = sg.student_id\n" +
                "left join groups as g on g.group_id = sg.group_id\n" +
                "left join speciality as spe on spe.speciality_id = g.speciality_id\n" +
                "left join s_cafedra as sc on sc.cafedra_id = spe.cafedra_id\n" +
                "left join payment as pay on pay.contract_id = c.contract_id\n" +
                "left join student_marks as sm on sm.student_id = s.student_id\n" +
                "left join s_mark as sma on sma.mark_id = sm.mark_id"
    )
    abstract fun getAllPersons(): Maybe<List<FullPerson>>

    @Query(
        "select \n" +
                "  p.person_id, \n" +
                "  p.surname, \n" +
                "  p.name, \n" +
                "  p.patronymic,\n" +
                "  p.sex,\n" +
                "  p.birth_date,\n" +
                "  p.birth_place,\n" +
                "  p.address,\n" +
                "  p.telephon,\n" +
                "  f.finance_name,\n" +
                "  ss.social_name,\n" +
                "  sp.privilege_name,\n" +
                "  pp.priv_begin_date,\n" +
                "  pp.priv_end_date,\n" +
                "  sd.diploma_name,\n" +
                "  sd.diploma_name,\n" +
                "  g.group_code,\n" +
                "  sg.putting_date,\n" +
                "  spe.speciality_name,\n" +
                "  sc.cafedra_name,\n" +
                "  sck.contract_kind_name,\n" +
                "  c.contract_date,\n" +
                "  pay.payment_date,\n" +
                "  pay.payment_sum,\n" +
                "  pay.payment_id,\n" +
                "  sma.mark_name,\n" +
                "  sm.id\n" +
                "from person as p\n" +
                "left join student as s on p.person_id = s.person_id \n" +
                "left join s_finance as f on s.finance_id = f.finance_id\n" +
                "left join s_social as ss on ss.social_id = p.social_id\n" +
                "left join person_privilege as pp on p.person_id = pp.person_id\n" +
                "left join s_privilege as sp on pp.privilege_id = sp.privilege_id\n" +
                "left join s_diploma as sd on s.diploma_id = sd.diploma_id\n" +
                "left join contract as c on c.student_id = s.student_id\n" +
                "left join s_contract_kind as sck on c.contract_kind_id = sck.contract_kind_id\n" +
                "left join student_group as sg on s.student_id = sg.student_id\n" +
                "left join groups as g on g.group_id = sg.group_id\n" +
                "left join speciality as spe on spe.speciality_id = g.speciality_id\n" +
                "left join s_cafedra as sc on sc.cafedra_id = spe.cafedra_id\n" +
                "left join payment as pay on pay.contract_id = c.contract_id\n" +
                "left join student_marks as sm on sm.student_id = s.student_id\n" +
                "left join s_mark as sma on sma.mark_id = sm.mark_id\n" +
                "where ss.social_name = :string\n" +
                "order by sp.privilege_name"
    )
    abstract fun getAllPersonsBySocialName(string: String): Maybe<List<FullPerson>>

    @Query(
        "select \n" +
                "  p.person_id, \n" +
                "  p.surname, \n" +
                "  p.name, \n" +
                "  p.patronymic,\n" +
                "  p.sex,\n" +
                "  p.birth_date,\n" +
                "  p.birth_place,\n" +
                "  p.address,\n" +
                "  p.telephon,\n" +
                "  f.finance_name,\n" +
                "  ss.social_name,\n" +
                "  sp.privilege_name,\n" +
                "  pp.priv_begin_date,\n" +
                "  pp.priv_end_date,\n" +
                "  sd.diploma_name,\n" +
                "  sd.diploma_name,\n" +
                "  g.group_code,\n" +
                "  sg.putting_date,\n" +
                "  spe.speciality_name,\n" +
                "  sc.cafedra_name,\n" +
                "  sck.contract_kind_name,\n" +
                "  c.contract_date,\n" +
                "  pay.payment_date,\n" +
                "  pay.payment_sum,\n" +
                "  pay.payment_id,\n" +
                "  sma.mark_name,\n" +
                "  sm.id\n" +
                "from person as p\n" +
                "left join student as s on p.person_id = s.person_id \n" +
                "left join s_finance as f on s.finance_id = f.finance_id\n" +
                "left join s_social as ss on ss.social_id = p.social_id\n" +
                "left join person_privilege as pp on p.person_id = pp.person_id\n" +
                "left join s_privilege as sp on pp.privilege_id = sp.privilege_id\n" +
                "left join s_diploma as sd on s.diploma_id = sd.diploma_id\n" +
                "left join contract as c on c.student_id = s.student_id\n" +
                "left join s_contract_kind as sck on c.contract_kind_id = sck.contract_kind_id\n" +
                "left join student_group as sg on s.student_id = sg.student_id\n" +
                "left join groups as g on g.group_id = sg.group_id\n" +
                "left join speciality as spe on spe.speciality_id = g.speciality_id\n" +
                "left join s_cafedra as sc on sc.cafedra_id = spe.cafedra_id\n" +
                "left join payment as pay on pay.contract_id = c.contract_id\n" +
                "left join student_marks as sm on sm.student_id = s.student_id\n" +
                "left join s_mark as sma on sma.mark_id = sm.mark_id\n" +
                "where f.finance_id = 2 and pay.payment_date = :date"
    )
    abstract fun getAllPersonsByPaymentDate(date: String): Maybe<List<FullPerson>>
}