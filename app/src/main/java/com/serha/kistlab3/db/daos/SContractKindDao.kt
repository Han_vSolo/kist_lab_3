package com.serha.kistlab3.db.daos

import android.arch.persistence.room.Dao
import com.serha.kistlab3.db.entities.SContractKind

@Dao
abstract class SContractKindDao : BaseDao<SContractKind> {

}