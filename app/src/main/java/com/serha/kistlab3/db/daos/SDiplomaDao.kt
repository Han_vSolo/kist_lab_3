package com.serha.kistlab3.db.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.serha.kistlab3.db.entities.SDiploma
import io.reactivex.Maybe

@Dao
abstract class SDiplomaDao : BaseDao<SDiploma> {

    @Query(
        "select \n" +
                "  *\n" +
                "from s_diploma"
    )
    abstract fun getAllDiplomas(): Maybe<List<SDiploma>>
}