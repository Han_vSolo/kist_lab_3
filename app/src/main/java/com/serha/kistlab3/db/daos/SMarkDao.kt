package com.serha.kistlab3.db.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.serha.kistlab3.db.entities.FullMarks
import com.serha.kistlab3.db.entities.SMark
import io.reactivex.Maybe

@Dao
abstract class SMarkDao : BaseDao<SMark> {

    @Query(
        "select\n" +
                "sma.mark_name\n" +
                "from person as p\n" +
                "left join student as s on p.person_id = s.person_id\n" +
                "left join s_diploma as sd on s.diploma_id = sd.diploma_id\n" +
                "left join student_marks as sm on sm.student_id = s.student_id\n" +
                "left join s_mark as sma on sma.mark_id = sm.mark_id\n" +
                "where sd.diploma_name = :string"
    )
    abstract fun getAllMarksByDiploma(string: String): Maybe<List<FullMarks>>
}