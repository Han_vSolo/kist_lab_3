package com.serha.kistlab3.db.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.serha.kistlab3.db.entities.SSocial
import io.reactivex.Maybe

@Dao
abstract class SSocialDao : BaseDao<SSocial> {

    @Query(
        "select \n" +
                "  *\n" +
                "from s_social as s"
    )
    abstract fun getAllSocialNames(): Maybe<List<SSocial>>
}