package com.serha.kistlab3.db.daos

import android.arch.persistence.room.Dao
import com.serha.kistlab3.db.entities.Speciality

@Dao
abstract class SpecialityDao : BaseDao<Speciality> {

}