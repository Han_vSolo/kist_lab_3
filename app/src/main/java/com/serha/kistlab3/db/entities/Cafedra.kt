package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "s_cafedra")
data class Cafedra(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "cafedra_id") val id: Long,
    @ColumnInfo(name = "cafedra_name") var name: String,
    @ColumnInfo(name = "cafedra_shiffr") var shiffr: String
)