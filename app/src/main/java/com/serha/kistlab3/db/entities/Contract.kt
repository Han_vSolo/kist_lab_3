package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(
    tableName = "contract",
    foreignKeys = [ForeignKey(
        entity = Student::class,
        parentColumns = arrayOf("student_id"),
        childColumns = arrayOf("student_id"),
        onDelete = ForeignKey.CASCADE
    ),
        ForeignKey(
            entity = SContractKind::class,
            parentColumns = arrayOf("contract_kind_id"),
            childColumns = arrayOf("contract_kind_id"),
            onDelete = ForeignKey.CASCADE
        )]
)
data class Contract(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "contract_id") val id: Long,
    @ColumnInfo(name = "student_id") var studentId: Long,
    @ColumnInfo(name = "contract_kind_id") var contractKindId: Long,
    @ColumnInfo(name = "contract_date") var contractDate: String,
    @ColumnInfo(name = "contract_no") var contractNo: Long,
    @ColumnInfo(name = "contract_Sum") var contractSum: Long,
    @ColumnInfo(name = "payer_kind") var payerKind: String
)