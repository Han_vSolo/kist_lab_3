package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo

class FullGroup {

    @ColumnInfo(name = "group_code")
    var group_code: String = ""

    @ColumnInfo(name = "group_creat_date")
    var group_creat_date: String = ""
}