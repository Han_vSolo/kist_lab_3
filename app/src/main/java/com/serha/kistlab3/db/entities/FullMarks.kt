package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo

class FullMarks {

    @ColumnInfo(name = "mark_name")
    var mark_name: String = ""
}