package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo

class FullPerson {

    @ColumnInfo(name = "person_id")
    var person_id: Long = 0

    @ColumnInfo(name = "surname")
    var surname: String = ""

    @ColumnInfo(name = "name")
    var name: String = ""

    @ColumnInfo(name = "patronymic")
    var patronymic: String = ""

    @ColumnInfo(name = "sex")
    var sex: String = ""

    @ColumnInfo(name = "birth_date")
    var birth_date: String = ""

    @ColumnInfo(name = "birth_place")
    var birth_place: String = ""

    @ColumnInfo(name = "address")
    var address: String = ""

    @ColumnInfo(name = "telephon")
    var telephon: String = ""

    @ColumnInfo(name = "finance_name")
    var finance_name: String = ""

    @ColumnInfo(name = "social_name")
    var social_name: String = ""

    @ColumnInfo(name = "privilege_name")
    var privilege_name: String = ""

    @ColumnInfo(name = "priv_begin_date")
    var priv_begin_date: String = ""

    @ColumnInfo(name = "priv_end_date")
    var priv_end_date: String = ""

    @ColumnInfo(name = "diploma_name")
    var diploma_name: String = ""

    @ColumnInfo(name = "group_code")
    var group_code: String = ""

    @ColumnInfo(name = "putting_date")
    var putting_date: String = ""

    @ColumnInfo(name = "speciality_name")
    var speciality_name: String = ""

    @ColumnInfo(name = "cafedra_name")
    var cafedra_name: String = ""

    @ColumnInfo(name = "contract_kind_name")
    var contract_kind_name: String = ""

    @ColumnInfo(name = "contract_date")
    var contract_date: String = ""

    @ColumnInfo(name = "payment_date")
    var payment_date: String = ""

    @ColumnInfo(name = "payment_sum")
    var payment_sum: Long = 0

    @ColumnInfo(name = "payment_id")
    var payment_id: Long = 0

    @ColumnInfo(name = "mark_name")
    var mark_name: String = ""

    @ColumnInfo(name = "id")
    var markId: Long = 0
}