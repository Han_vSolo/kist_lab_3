package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(
    tableName = "groups",
    foreignKeys = [ForeignKey(
        entity = Speciality::class,
        parentColumns = arrayOf("speciality_id"),
        childColumns = arrayOf("speciality_id"),
        onDelete = ForeignKey.CASCADE
    )]
)
data class Groups(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "group_id") val id: Long,
    @ColumnInfo(name = "speciality_id") var specialityId: Long,
    @ColumnInfo(name = "group_code") var groupCode: String,
    @ColumnInfo(name = "group_creat_date") var groupCreatDate: String
)