package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "payment")
data class Payment(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "payment_id") val id: Long,
    @ColumnInfo(name = "contract_id") var contractId: Long,
    @ColumnInfo(name = "payment_date") var paymentDate: String,
    @ColumnInfo(name = "payment_sum") var paymentSum: Long,
    @ColumnInfo(name = "document_no") var documentNo: Long
)