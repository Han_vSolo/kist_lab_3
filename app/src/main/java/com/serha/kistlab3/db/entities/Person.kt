package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(
    tableName = "person",
    foreignKeys = [ForeignKey(
        entity = SSocial::class,
        parentColumns = arrayOf("social_id"),
        childColumns = arrayOf("social_id"),
        onDelete = ForeignKey.CASCADE
    )]
)
data class Person(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "person_id") val id: Long,
    @ColumnInfo(name = "surname") var surname: String,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "patronymic") var patronymic: String,
    @ColumnInfo(name = "birth_date") var birthDate: String,
    @ColumnInfo(name = "sex") var sex: String,
    @ColumnInfo(name = "birth_place") var birthPlace: String,
    @ColumnInfo(name = "address") var address: String,
    @ColumnInfo(name = "telephon") var telephon: String,
    @ColumnInfo(name = "social_id") var socialId: Long
)