package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(
    tableName = "person_privilege",
    foreignKeys = [ForeignKey(
        entity = Person::class,
        parentColumns = arrayOf("person_id"),
        childColumns = arrayOf("person_id"),
        onDelete = ForeignKey.CASCADE
    ),
        ForeignKey(
            entity = SPrivilege::class,
            parentColumns = arrayOf("privilege_id"),
            childColumns = arrayOf("privilege_id"),
            onDelete = ForeignKey.CASCADE
        )]
)
data class PersonPrivilege(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Long,
    @ColumnInfo(name = "person_id") var personId: Long,
    @ColumnInfo(name = "privilege_id") var privilegeId: Long,
    @ColumnInfo(name = "priv_begin_date") var beginDate: String,
    @ColumnInfo(name = "priv_end_date") var endDate: String,
    @ColumnInfo(name = "ground") var ground: String
)