package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "s_contract_kind")
data class SContractKind(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "contract_kind_id") val id: Long,
    @ColumnInfo(name = "contract_kind_name") var name: String
)