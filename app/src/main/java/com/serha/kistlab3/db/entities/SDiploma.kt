package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "s_diploma")
data class SDiploma(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "diploma_id") val id: Long,
    @ColumnInfo(name = "diploma_name") var name: String
)