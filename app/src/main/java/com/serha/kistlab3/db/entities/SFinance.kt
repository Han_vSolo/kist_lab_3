package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "s_finance")
data class SFinance(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "finance_id") val id: Long,
    @ColumnInfo(name = "finance_name") var name: String
)