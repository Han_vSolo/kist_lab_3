package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "s_mark")
data class SMark(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "mark_id") val id: Long,
    @ColumnInfo(name = "mark_name") var name: String
)