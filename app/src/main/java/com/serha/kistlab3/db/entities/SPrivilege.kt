package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "s_privilege")
data class SPrivilege(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "privilege_id") val id: Long,
    @ColumnInfo(name = "privilege_name") var name: String
)