package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "s_social")
class SSocial(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "social_id") val id: Long,
    @ColumnInfo(name = "social_name") var name: String
)