package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(
    tableName = "speciality",
    foreignKeys = [ForeignKey(
        entity = Cafedra::class,
        parentColumns = arrayOf("cafedra_id"),
        childColumns = arrayOf("cafedra_id"),
        onDelete = ForeignKey.CASCADE
    )]
)
data class Speciality(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "speciality_id") val id: Long,
    @ColumnInfo(name = "cafedra_id") var cafedraId: Long,
    @ColumnInfo(name = "speciality_name") var specialityName: String,
    @ColumnInfo(name = "speciality_shifr") var specialityShifr: String
)