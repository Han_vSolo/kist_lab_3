package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(
    tableName = "student",
    foreignKeys = [ForeignKey(
        entity = SFinance::class,
        parentColumns = arrayOf("finance_id"),
        childColumns = arrayOf("finance_id"),
        onDelete = ForeignKey.CASCADE
    ),
        ForeignKey(
            entity = SDiploma::class,
            parentColumns = arrayOf("diploma_id"),
            childColumns = arrayOf("diploma_id"),
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Person::class,
            parentColumns = arrayOf("person_id"),
            childColumns = arrayOf("person_id"),
            onDelete = ForeignKey.CASCADE
        )]
)
data class Student(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "student_id") val id: Long,
    @ColumnInfo(name = "person_id") var personId: Long,
    @ColumnInfo(name = "finance_id") var financeId: Long,
    @ColumnInfo(name = "diploma_id") var diplomaId: Long,
    @ColumnInfo(name = "book_no") var bookNo: Long,
    @ColumnInfo(name = "note") var note: String
)