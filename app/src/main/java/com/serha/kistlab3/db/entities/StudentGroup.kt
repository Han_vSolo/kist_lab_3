package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(
    tableName = "student_group",
    foreignKeys = [ForeignKey(
        entity = Groups::class,
        parentColumns = arrayOf("group_id"),
        childColumns = arrayOf("group_id"),
        onDelete = ForeignKey.CASCADE
    ),
        ForeignKey(
            entity = Student::class,
            parentColumns = arrayOf("student_id"),
            childColumns = arrayOf("student_id"),
            onDelete = ForeignKey.CASCADE
        )]
)
data class StudentGroup(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Long,
    @ColumnInfo(name = "student_id") var studentId: Long,
    @ColumnInfo(name = "group_id") var groupId: Long,
    @ColumnInfo(name = "putting_date") var puttingDate: String
)