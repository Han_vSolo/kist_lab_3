package com.serha.kistlab3.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(
    tableName = "student_marks",
    foreignKeys = [ForeignKey(
        entity = Student::class,
        parentColumns = arrayOf("student_id"),
        childColumns = arrayOf("student_id"),
        onDelete = ForeignKey.CASCADE
    ),
        ForeignKey(
            entity = SMark::class,
            parentColumns = arrayOf("mark_id"),
            childColumns = arrayOf("mark_id"),
            onDelete = ForeignKey.CASCADE
        )]
)
data class StudentMarks(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Long,
    @ColumnInfo(name = "student_id") var studentId: Long,
    @ColumnInfo(name = "mark_id") var markId: Long
)