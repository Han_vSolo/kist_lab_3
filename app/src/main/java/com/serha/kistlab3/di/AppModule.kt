package com.serha.kistlab3.di

import android.content.Context
import android.preference.PreferenceManager
import com.serha.kistlab3.App
import com.serha.kistlab3.db.AppDatabase
import com.serha.kistlab3.db.daos.*
import com.serha.kistlab3.helpers.RxRoomHelper
import com.serha.kistlab3.mvp.contracts.ChooseContract
import com.serha.kistlab3.mvp.contracts.ContentContract
import com.serha.kistlab3.mvp.contracts.ParametrsContract
import com.serha.kistlab3.mvp.models.ChooseModel
import com.serha.kistlab3.mvp.models.ContentModel
import com.serha.kistlab3.mvp.models.ParametrsModel
import com.serha.kistlab3.mvp.presenters.ChoosePresenter
import com.serha.kistlab3.mvp.presenters.ContentPresenter
import com.serha.kistlab3.mvp.presenters.ParametrsPresenter
import org.koin.dsl.module.module

val appModule = module {

    module("db") {

        factory<AppDatabase> { App.mInstance.database }

        factory<CafedraDao> { get<AppDatabase>().cafedraDao() }

        factory<ContractDao> { get<AppDatabase>().contractDao() }

        factory<GroupsDao> { get<AppDatabase>().groupsDao() }

        factory<PaymentDao> { get<AppDatabase>().paymentDao() }

        factory<PersonDao>() { get<AppDatabase>().personDao() }

        factory<PersonPrivilegeDao> { get<AppDatabase>().personPrivilegeDao() }

        factory<SContractKindDao> { get<AppDatabase>().sContractKindDao() }

        factory<SDiplomaDao> { get<AppDatabase>().sDiplomaDao() }

        factory<SFinanceDao> { get<AppDatabase>().sFinanceDao() }

        factory<SMarkDao> { get<AppDatabase>().sMarkDao() }

        factory<SpecialityDao> { get<AppDatabase>().specialityDao() }

        factory<SPrivilegeDao> { get<AppDatabase>().sPrivilegeDao() } //bind SPrivilegeDao::class

        factory<SSocialDao> { get<AppDatabase>().sSocialDao() }

        factory<StudentDao> { get<AppDatabase>().studentDao() }

        factory<StudentGroupDao> { get<AppDatabase>().studentGroupDao() }

        factory<StudentMarksDao> { get<AppDatabase>().studentMarksDao() }

        single {
            RxRoomHelper(
                get(),
                get(),
                get(),
                get(),
                get(),
                get(),
                get(),
                get(),
                get(),
                get(),
                get(),
                get(),
                get(),
                get(),
                get(),
                get()
            )
        }

        module("mvp") {

            factory { ChooseModel() } bind ChooseContract.Model::class

            scope("chooseSession") { ChoosePresenter(get()) } bind ChooseContract.Presenter::class

            factory { ContentModel(get(), get(), get()) } bind ContentContract.Model::class

            scope("contentSession") { ContentPresenter(get()) } bind ContentContract.Presenter::class

            factory { ParametrsModel(get(), get(), get(), get()) } bind ParametrsContract.Model::class

            scope("parametrSession") { ParametrsPresenter(get()) } bind ParametrsContract.Presenter::class
        }
    }

    module("preferences") {

        factory { (context: Context) -> PreferenceManager.getDefaultSharedPreferences(context) }
    }

    module("adapters") {

        //        factory { (contents: ArrayList<FullPersonModel>, context: BaseActivity) -> ContentAdapter(contents, context) }
    }

//    module("retrofit"){
//
//        single(name = "vo-hub") { RetrofitFactory.getInstanceJSON(RetrofitService.ENDPOINT).create(RetrofitService::class.java) }
//
//        single(name = "vo.org") { RetrofitFactory.getInstanceJSON(RetrofitServiceVoOrg.ENDPOINT).create(RetrofitService::class.java) }
//
//        single(name = "ntp") { RetrofitFactory.getInstanceJSON(RetrofitServiceNTPServer.ENDPOINT).create(RetrofitService::class.java) }
//
//        single(name = "crm") { RetrofitFactory.getInstanceJSON(RetrofitServiceVoCrmOrg.ENDPOINT).create(RetrofitService::class.java) }
//
//        single(name = "weather") { RetrofitFactory.getInstanceJSON(RetrofitServiceOpenWeatherMap.ENDPOINT).create(RetrofitService::class.java) }
//
//        factory(name = "endpoint") { (endpoint: String) -> RetrofitFactory.getInstanceJSON(endpoint).create(RetrofitService::class.java) }
//
//        factory(name = "xml") { (endpoint: String) -> RetrofitFactory.getInstanceXML(endpoint).create(RetrofitServiceRSS::class.java) }
//
//        single(name = "session") { RetrofitFactory.getInstanceWithSessionId(App.getInstance()).create(RetrofitService::class.java) }
//    }
}