package com.serha.kistlab3.helpers

interface OnLoadComplete {

    fun onLoadComplete()
}