package com.serha.kistlab3.helpers

import com.serha.kistlab3.db.daos.*
import com.serha.kistlab3.db.entities.*
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class RxRoomHelper(
    val cafedraDao: CafedraDao,
    val contractDao: ContractDao,
    val groupsDao: GroupsDao,
    val paymentDao: PaymentDao,
    val personDao: PersonDao,
    val personPrivilegeDao: PersonPrivilegeDao,
    val sContractKindDao: SContractKindDao,
    val sDiplomaDao: SDiplomaDao,
    val sFinanceDao: SFinanceDao,
    val sMarkDao: SMarkDao,
    val specialityDao: SpecialityDao,
    val sPrivilegeDao: SPrivilegeDao,
    val sSocialDao: SSocialDao,
    val studentDao: StudentDao,
    val studentGroupDao: StudentGroupDao,
    val studentMarksDao: StudentMarksDao
) {

    fun insertData(data: Cafedra) {
        Single.fromCallable {
            cafedraDao.insert(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertCafedraData(data: List<Cafedra>) {
        Single.fromCallable {
            cafedraDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertCafedraData(data: List<Cafedra>, listener: OnLoadComplete) {
        Single.fromCallable {
            cafedraDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { listener.onLoadComplete() }
            .subscribe()
//        val observer = object : Observer<List<Cafedra>> {
//            override fun onSubscribe(d: Disposable) {
//
//            }
//
//            override fun onNext(word: List<Cafedra>) {
//                cafedraDao.insertAll(data)
//            }
//
//            override fun onError(e: Throwable) {
//
//            }
//
//            override fun onComplete() {
//                listener.onLoadComplete()
//            }
//        }
//        val observable = object : Observable<List<Cafedra>>() {
//            override fun subscribeActual(observer: Observer<in List<Cafedra>>) {
//                observer.onNext(data)
//            }
//        }
//        observable
//            .subscribeOn(Schedulers.io())
//            .subscribe(observer)
    }

    fun insertData(data: Contract) {
        Single.fromCallable {
            contractDao.insert(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertContractData(data: List<Contract>) {
        Single.fromCallable {
            contractDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertContractData(data: List<Contract>, listener: OnLoadComplete) {
        Single.fromCallable {
            contractDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { listener.onLoadComplete() }
            .subscribe()

//        val observer = object : Observer<List<Contract>> {
//            override fun onSubscribe(d: Disposable) {
//
//            }
//
//            override fun onNext(word: List<Contract>) {
//                contractDao.insertAll(data)
//            }
//
//            override fun onError(e: Throwable) {
//
//            }
//
//            override fun onComplete() {
//                listener.onLoadComplete()
//            }
//        }
//        val observable = object : Observable<List<Contract>>() {
//            override fun subscribeActual(observer: Observer<in List<Contract>>) {
//                observer.onNext(data)
//            }
//        }
//        observable
//            .subscribeOn(Schedulers.io())
//            .subscribe(observer)
    }

    fun insertData(data: Groups) {
        Single.fromCallable {
            groupsDao.insert(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertGroupsData(data: List<Groups>) {
        Single.fromCallable {
            groupsDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertGroupsData(data: List<Groups>, listener: OnLoadComplete) {
        Single.fromCallable {
            groupsDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { listener.onLoadComplete() }
            .subscribe()

//        val observer = object : Observer<List<Groups>> {
//            override fun onSubscribe(d: Disposable) {
//
//            }
//
//            override fun onNext(word: List<Groups>) {
//                groupsDao.insertAll(data)
//            }
//
//            override fun onError(e: Throwable) {
//
//            }
//
//            override fun onComplete() {
//                listener.onLoadComplete()
//            }
//        }
//        val observable = object : Observable<List<Groups>>() {
//            override fun subscribeActual(observer: Observer<in List<Groups>>) {
//                observer.onNext(data)
//            }
//        }
//        observable
//            .subscribeOn(Schedulers.io())
//            .subscribe(observer)
    }

    fun insertData(data: Payment) {
        Single.fromCallable {
            paymentDao.insert(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertPaymentData(data: List<Payment>) {
        Single.fromCallable {
            paymentDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertPaymentData(data: List<Payment>, listener: OnLoadComplete) {

        Single.fromCallable {
            paymentDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { listener.onLoadComplete() }
            .subscribe()

//        val observer = object : Observer<List<Payment>> {
//            override fun onSubscribe(d: Disposable) {
//
//            }
//
//            override fun onNext(word: List<Payment>) {
//                paymentDao.insertAll(data)
//            }
//
//            override fun onError(e: Throwable) {
//
//            }
//
//            override fun onComplete() {
//                listener.onLoadComplete()
//            }
//        }
//        val observable = object : Observable<List<Payment>>() {
//            override fun subscribeActual(observer: Observer<in List<Payment>>) {
//                observer.onNext(data)
//            }
//        }
//        observable
//            .subscribeOn(Schedulers.io())
//            .subscribe(observer)
    }

    fun insertData(data: Person) {
        Single.fromCallable {
            personDao.insert(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertPersonData(data: List<Person>) {
        Single.fromCallable {
            personDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertPersonData(data: List<Person>, listener: OnLoadComplete) {
        Single.fromCallable {
            personDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { listener.onLoadComplete() }
            .subscribe()

//        val observer = object : Observer<List<Person>> {
//            override fun onSubscribe(d: Disposable) {
//
//            }
//
//            override fun onNext(word: List<Person>) {
//                personDao.insertAll(data)
//            }
//
//            override fun onError(e: Throwable) {
//
//            }
//
//            override fun onComplete() {
//                listener.onLoadComplete()
//            }
//        }
//        val observable = object : Observable<List<Person>>() {
//            override fun subscribeActual(observer: Observer<in List<Person>>) {
//                observer.onNext(data)
//            }
//        }
//        observable
//            .subscribeOn(Schedulers.io())
//            .subscribe(observer)
    }

    fun insertData(data: PersonPrivilege) {
        Single.fromCallable {
            personPrivilegeDao.insert(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertPersonPrivilegeData(data: List<PersonPrivilege>) {
        Single.fromCallable {
            personPrivilegeDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertPersonPrivilegeData(data: List<PersonPrivilege>, listener: OnLoadComplete) {
        Single.fromCallable {
            personPrivilegeDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { listener.onLoadComplete() }
            .subscribe()

//        val observer = object : Observer<List<PersonPrivilege>> {
//            override fun onSubscribe(d: Disposable) {
//
//            }
//
//            override fun onNext(word: List<PersonPrivilege>) {
//                personPrivilegeDao.insertAll(data)
//            }
//
//            override fun onError(e: Throwable) {
//
//            }
//
//            override fun onComplete() {
//                listener.onLoadComplete()
//            }
//        }
//        val observable = object : Observable<List<PersonPrivilege>>() {
//            override fun subscribeActual(observer: Observer<in List<PersonPrivilege>>) {
//                observer.onNext(data)
//            }
//        }
//        observable
//            .subscribeOn(Schedulers.io())
//            .subscribe(observer)
    }

    fun insertData(data: SContractKind) {
        Single.fromCallable {
            sContractKindDao.insert(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertSContractKindData(data: List<SContractKind>) {
        Single.fromCallable {
            sContractKindDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertSContractKindData(data: List<SContractKind>, listener: OnLoadComplete) {
        Single.fromCallable {
            sContractKindDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { listener.onLoadComplete() }
            .subscribe()

//        val observer = object : Observer<List<SContractKind>> {
//            override fun onSubscribe(d: Disposable) {
//
//            }
//
//            override fun onNext(word: List<SContractKind>) {
//                sContractKindDao.insertAll(data)
//            }
//
//            override fun onError(e: Throwable) {
//
//            }
//
//            override fun onComplete() {
//                listener.onLoadComplete()
//            }
//        }
//        val observable = object : Observable<List<SContractKind>>() {
//            override fun subscribeActual(observer: Observer<in List<SContractKind>>) {
//                observer.onNext(data)
//            }
//        }
//        observable
//            .subscribeOn(Schedulers.io())
//            .subscribe(observer)
    }

    fun insertData(data: SDiploma) {
        Single.fromCallable {
            sDiplomaDao.insert(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertSDiplomaData(data: List<SDiploma>) {
        Single.fromCallable {
            sDiplomaDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertSDiplomaData(data: List<SDiploma>, listener: OnLoadComplete) {
        Single.fromCallable {
            sDiplomaDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { listener.onLoadComplete() }
            .subscribe()

//        val observer = object : Observer<List<SDiploma>> {
//            override fun onSubscribe(d: Disposable) {
//
//            }
//
//            override fun onNext(word: List<SDiploma>) {
//                sDiplomaDao.insertAll(data)
//            }
//
//            override fun onError(e: Throwable) {
//
//            }
//
//            override fun onComplete() {
//                listener.onLoadComplete()
//            }
//        }
//        val observable = object : Observable<List<SDiploma>>() {
//            override fun subscribeActual(observer: Observer<in List<SDiploma>>) {
//                observer.onNext(data)
//            }
//        }
//        observable
//            .subscribeOn(Schedulers.io())
//            .subscribe(observer)
    }

    fun insertData(data: SFinance) {
        Single.fromCallable {
            sFinanceDao.insert(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertSFinanceData(data: List<SFinance>) {
        Single.fromCallable {
            sFinanceDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertSFinanceData(data: List<SFinance>, listener: OnLoadComplete) {
        Single.fromCallable {
            sFinanceDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { listener.onLoadComplete() }
            .subscribe()

//        val observer = object : Observer<List<SFinance>> {
//            override fun onSubscribe(d: Disposable) {
//
//            }
//
//            override fun onNext(word: List<SFinance>) {
//                sFinanceDao.insertAll(data)
//            }
//
//            override fun onError(e: Throwable) {
//
//            }
//
//            override fun onComplete() {
//                listener.onLoadComplete()
//            }
//        }
//        val observable = object : Observable<List<SFinance>>() {
//            override fun subscribeActual(observer: Observer<in List<SFinance>>) {
//                observer.onNext(data)
//            }
//        }
//        observable
//            .subscribeOn(Schedulers.io())
//            .subscribe(observer)
    }

    fun insertData(data: SMark) {
        Single.fromCallable {
            sMarkDao.insert(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertSMarkData(data: List<SMark>) {
        Single.fromCallable {
            sMarkDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertSMarkData(data: List<SMark>, listener: OnLoadComplete) {
        Single.fromCallable {
            sMarkDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { listener.onLoadComplete() }
            .subscribe()

//        val observer = object : Observer<List<SMark>> {
//            override fun onSubscribe(d: Disposable) {
//
//            }
//
//            override fun onNext(word: List<SMark>) {
//                sMarkDao.insertAll(data)
//            }
//
//            override fun onError(e: Throwable) {
//
//            }
//
//            override fun onComplete() {
//                listener.onLoadComplete()
//            }
//        }
//        val observable = object : Observable<List<SMark>>() {
//            override fun subscribeActual(observer: Observer<in List<SMark>>) {
//                observer.onNext(data)
//            }
//        }
//        observable
//            .subscribeOn(Schedulers.io())
//            .subscribe(observer)
    }

    fun insertData(data: Speciality) {
        Single.fromCallable {
            specialityDao.insert(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertSpecialityData(data: List<Speciality>) {
        Single.fromCallable {
            specialityDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertSpecialityData(data: List<Speciality>, listener: OnLoadComplete) {
        Single.fromCallable {
            specialityDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { listener.onLoadComplete() }
            .subscribe()

//        val observer = object : Observer<List<Speciality>> {
//            override fun onSubscribe(d: Disposable) {
//
//            }
//
//            override fun onNext(word: List<Speciality>) {
//                specialityDao.insertAll(data)
//            }
//
//            override fun onError(e: Throwable) {
//
//            }
//
//            override fun onComplete() {
//                listener.onLoadComplete()
//            }
//        }
//        val observable = object : Observable<List<Speciality>>() {
//            override fun subscribeActual(observer: Observer<in List<Speciality>>) {
//                observer.onNext(data)
//            }
//        }
//        observable
//            .subscribeOn(Schedulers.io())
//            .subscribe(observer)
    }

    fun insertData(data: SPrivilege) {
        Single.fromCallable {
            sPrivilegeDao.insert(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertSPrivilegeData(data: List<SPrivilege>) {
        Single.fromCallable {
            sPrivilegeDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertSPrivilegeData(data: List<SPrivilege>, listener: OnLoadComplete) {
        Single.fromCallable {
            sPrivilegeDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { listener.onLoadComplete() }
            .subscribe()
//        val observer = object : Observer<List<SPrivilege>> {
//            override fun onSubscribe(d: Disposable) {
//
//            }
//
//            override fun onNext(word: List<SPrivilege>) {
//                sPrivilegeDao.insertAll(data)
//            }
//
//            override fun onError(e: Throwable) {
//
//            }
//
//            override fun onComplete() {
//                listener.onLoadComplete()
//            }
//        }
//        val observable = object : Observable<List<SPrivilege>>() {
//            override fun subscribeActual(observer: Observer<in List<SPrivilege>>) {
//                observer.onNext(data)
//            }
//        }
//        observable
//            .subscribeOn(Schedulers.io())
//            .subscribe(observer)
    }

    fun insertData(data: SSocial) {
        Single.fromCallable {
            sSocialDao.insert(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertSSocialData(data: List<SSocial>) {
        Single.fromCallable {
            sSocialDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertSSocialData(data: List<SSocial>, listener: OnLoadComplete) {
        Single.fromCallable {
            sSocialDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { listener.onLoadComplete() }
            .subscribe()

//        val observer = object : Observer<List<SSocial>> {
//            override fun onSubscribe(d: Disposable) {
//
//            }
//
//            override fun onNext(word: List<SSocial>) {
//                sSocialDao.insertAll(data)
//            }
//
//            override fun onError(e: Throwable) {
//
//            }
//
//            override fun onComplete() {
//                listener.onLoadComplete()
//            }
//        }
//        val observable = object : Observable<List<SSocial>>() {
//            override fun subscribeActual(observer: Observer<in List<SSocial>>) {
//                observer.onNext(data)
//            }
//        }
//        observable
//            .subscribeOn(Schedulers.io())
//            .subscribe(observer)
    }

    fun insertData(data: Student) {
        Single.fromCallable {
            studentDao.insert(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertStudentData(data: List<Student>) {
        Single.fromCallable {
            studentDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertStudentData(data: List<Student>, listener: OnLoadComplete) {
        Single.fromCallable {
            studentDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { listener.onLoadComplete() }
            .subscribe()

//        val observer = object : Observer<List<Student>> {
//            override fun onSubscribe(d: Disposable) {
//
//            }
//
//            override fun onNext(word: List<Student>) {
//                studentDao.insertAll(data)
//            }
//
//            override fun onError(e: Throwable) {
//
//            }
//
//            override fun onComplete() {
//                listener.onLoadComplete()
//            }
//        }
//        val observable = object : Observable<List<Student>>() {
//            override fun subscribeActual(observer: Observer<in List<Student>>) {
//                observer.onNext(data)
//            }
//        }
//        observable
//            .subscribeOn(Schedulers.io())
//            .subscribe(observer)
    }

    fun insertData(data: StudentGroup) {
        Single.fromCallable {
            studentGroupDao.insert(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertStudentGroupData(data: List<StudentGroup>) {
        Single.fromCallable {
            studentGroupDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertStudentGroupData(data: List<StudentGroup>, listener: OnLoadComplete) {
        Single.fromCallable {
            studentGroupDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { listener.onLoadComplete() }
            .subscribe()

//        val observer = object : Observer<List<StudentGroup>> {
//            override fun onSubscribe(d: Disposable) {
//
//            }
//
//            override fun onNext(word: List<StudentGroup>) {
//                studentGroupDao.insertAll(data)
//            }
//
//            override fun onError(e: Throwable) {
//
//            }
//
//            override fun onComplete() {
//                listener.onLoadComplete()
//            }
//        }
//        val observable = object : Observable<List<StudentGroup>>() {
//            override fun subscribeActual(observer: Observer<in List<StudentGroup>>) {
//                observer.onNext(data)
//            }
//        }
//        observable
//            .subscribeOn(Schedulers.io())
//            .subscribe(observer)
    }

    fun insertData(data: StudentMarks) {
        Single.fromCallable {
            studentMarksDao.insert(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertStudentMarksData(data: List<StudentMarks>) {
        Single.fromCallable {
            studentMarksDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun insertStudentMarksData(data: List<StudentMarks>, listener: OnLoadComplete) {
        Single.fromCallable {
            studentMarksDao.insertAll(data)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { listener.onLoadComplete() }
            .subscribe()

//        val observer = object : Observer<List<StudentMarks>> {
//            override fun onSubscribe(d: Disposable) {
//
//            }
//
//            override fun onNext(word: List<StudentMarks>) {
//                studentMarksDao.insertAll(data)
//            }
//
//            override fun onError(e: Throwable) {
//
//            }
//
//            override fun onComplete() {
//                listener.onLoadComplete()
//            }
//        }
//        val observable = object : Observable<List<StudentMarks>>() {
//            override fun subscribeActual(observer: Observer<in List<StudentMarks>>) {
//                observer.onNext(data)
//            }
//        }
//        observable
//            .subscribeOn(Schedulers.io())
//            .subscribe(observer)
    }
}