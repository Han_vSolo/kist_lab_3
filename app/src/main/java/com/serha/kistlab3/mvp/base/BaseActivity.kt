package com.serha.kistlab3.mvp.base

import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.serha.kistlab3.views.LoadingView

abstract class BaseActivity : AppCompatActivity(), BaseFragment.BackHandlerInterface, MvpView {
    private var subtitle: String? = ""

    var mSelectedFragment: BaseFragment? = null

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount === 1) {
            finish()
        } else {
            super.onBackPressed()
        }
    }

    fun clearBackStack() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack(
                supportFragmentManager.getBackStackEntryAt(0).id,
                FragmentManager.POP_BACK_STACK_INCLUSIVE
            )
        }
    }

    fun showFirstFragment(f: BaseFragment, fragmentTag: String = f.getFragmentTag()) {
        clearBackStack()

        showFragment(f, fragmentTag)
    }

    fun showFragment(f: BaseFragment, fragmentTag: String = f.getFragmentTag()) {
        if (getContainerID() < 0) {
            return
        }

        val fm = supportFragmentManager

        val fragmentTransaction = fm.beginTransaction()

        var fragment = fm.findFragmentByTag(fragmentTag)

        if (fragment == null) {
            fragment = f
            fragmentTransaction
                .replace(getContainerID(), f, fragmentTag)
                .addToBackStack(fragmentTag)
                .commit()
        } else {
            fm.popBackStack(fragmentTag, 0)
            fragment = fragment as BaseFragment
        }
    }

    fun setSubtitle(newSubtitle: String) {
        subtitle = newSubtitle
    }

    fun showSubTitle() {
        if (subtitle != null && !subtitle!!.isEmpty()) {
            val ab = supportActionBar
            ab?.subtitle = subtitle
        }
    }

    override fun setSelectedFragment(baseFragment: BaseFragment) {
        this.mSelectedFragment = baseFragment
    }

    fun getSelectedFragment(): BaseFragment? {
        return mSelectedFragment
    }

    protected open fun getContainerID(): Int {
        return -1
    }

    override fun showError(string: String) {
        Toast.makeText(applicationContext, string, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        LoadingView.show(applicationContext)
    }

    override fun hideProgress() {
        LoadingView.hide()
    }
}