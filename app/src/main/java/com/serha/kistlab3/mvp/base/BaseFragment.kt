package com.serha.kistlab3.mvp.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.widget.Toast
import com.serha.kistlab3.R
import com.serha.kistlab3.mvp.views.activities.MainActivity

abstract class BaseFragment : Fragment(), MvpView {

    private val title: String? = null
    private val subtitle: String? = null
    private var showBackButton = false

    protected var backHandlerInterface: BackHandlerInterface? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (activity !is BackHandlerInterface) {
            throw ClassCastException("Hosting activity must implement BackHandlerInterface")
        } else {
            backHandlerInterface = activity as BackHandlerInterface
        }
    }

    override fun onStart() {
        super.onStart()

        if (backHandlerInterface != null) {
            backHandlerInterface!!.setSelectedFragment(this)
        }
    }

    abstract fun onBackPressed(): Boolean

    fun getFragmentTag(): String {
        return this::class.java.simpleName
    }

    interface BackHandlerInterface {
        fun setSelectedFragment(baseFragment: BaseFragment)
    }

    fun setBackNavigation(showBackButton: Boolean) {
        val actionBar = (activity as AppCompatActivity).supportActionBar
        val toolbar = (activity as AppCompatActivity).findViewById(R.id.toolbar) as Toolbar
        this.showBackButton = showBackButton

        if (toolbar != null && actionBar != null && activity is MainActivity) {
            if (showBackButton) {
                actionBar.setHomeAsUpIndicator(R.drawable.icon_toolbal_arrow_white)
                toolbar.setNavigationOnClickListener {
                    if (activity != null) {
                        (activity as AppCompatActivity).onBackPressed()
                    }
                }
            }
        }
    }

    override fun showError(string: String) {
        Toast.makeText(context, string, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {

    }

    override fun hideProgress() {

    }
}