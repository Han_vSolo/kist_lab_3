package com.serha.kistlab3.mvp.base

interface ILoadCallback<T> {

    fun onLoad(items: List<T>)

    fun onNoData()

    fun onError(e: String)
}