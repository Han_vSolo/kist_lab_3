package com.serha.kistlab3.mvp.base

interface MvpView {

    fun showError(string: String)

    fun showProgress()

    fun hideProgress()
}