package com.serha.kistlab3.mvp.contracts

import com.serha.kistlab3.mvp.base.BaseActivity
import com.serha.kistlab3.mvp.base.MvpModel
import com.serha.kistlab3.mvp.base.MvpPresenter
import com.serha.kistlab3.mvp.base.MvpView

class ChooseContract {

    interface View : MvpView {

    }

    interface Presenter : MvpPresenter<View> {

        fun onClick(i: Int, activity: BaseActivity)
    }

    interface Model : MvpModel {

    }
}