package com.serha.kistlab3.mvp.contracts

import com.serha.kistlab3.FullPersonModel
import com.serha.kistlab3.mvp.base.ILoadCallback
import com.serha.kistlab3.mvp.base.MvpModel
import com.serha.kistlab3.mvp.base.MvpPresenter
import com.serha.kistlab3.mvp.base.MvpView

class ContentContract {

    interface View : MvpView {

        fun notifyDataSetChanged()
    }

    interface Presenter : MvpPresenter<View> {

        fun getData(): ArrayList<FullPersonModel>

        fun getDataByParametrs(type: Int, string: String)
    }

    interface Model : MvpModel {

        fun getPersonsFromDB(listener: ILoadCallback<FullPersonModel>)

        fun getPersonsFromDBByPayment(listener: ILoadCallback<FullPersonModel>, string: String)

        fun getPersonsFromDBBySocial(listener: ILoadCallback<FullPersonModel>, string: String)

        fun getAllGroups(listener: ILoadCallback<FullPersonModel>, string: String)

        fun getAllMarks(listener: ILoadCallback<FullPersonModel>, string: String)
    }
}