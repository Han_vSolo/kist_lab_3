package com.serha.kistlab3.mvp.contracts

import com.serha.kistlab3.helpers.OnLoadComplete
import com.serha.kistlab3.mvp.base.BaseActivity
import com.serha.kistlab3.mvp.base.MvpModel
import com.serha.kistlab3.mvp.base.MvpPresenter
import com.serha.kistlab3.mvp.base.MvpView

class ParametrsContract {

    interface View : MvpView {

    }

    interface Presenter : MvpPresenter<View> {

        fun getParametrs(type: Int)

        fun startQuery(type: Int, parametr: String, activity: BaseActivity)
    }

    interface Model : MvpModel {

        fun getAllSocials(listener: OnLoadComplete)

        fun getAllPayments(listener: OnLoadComplete)

        fun getAllCafedras(listener: OnLoadComplete)

        fun getAllDiplomas(listener: OnLoadComplete)
    }
}