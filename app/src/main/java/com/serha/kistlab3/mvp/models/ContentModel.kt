package com.serha.kistlab3.mvp.models

import com.serha.kistlab3.FullPersonModel
import com.serha.kistlab3.db.daos.GroupsDao
import com.serha.kistlab3.db.daos.PersonDao
import com.serha.kistlab3.db.daos.SMarkDao
import com.serha.kistlab3.db.entities.FullGroup
import com.serha.kistlab3.db.entities.FullMarks
import com.serha.kistlab3.db.entities.FullPerson
import com.serha.kistlab3.mvp.base.ILoadCallback
import com.serha.kistlab3.mvp.contracts.ContentContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableMaybeObserver
import io.reactivex.schedulers.Schedulers

class ContentModel(val personDao: PersonDao, val sMarkDao: SMarkDao, val groupsDao: GroupsDao) : ContentContract.Model {

    var contents = ArrayList<FullPersonModel>()

    override fun getAllGroups(listener: ILoadCallback<FullPersonModel>, string: String) {
        groupsDao.getGroupsByCafedra(string)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableMaybeObserver<List<FullGroup>>() {
                override fun onSuccess(list: List<FullGroup>) {
                    for (group in list) {
                        var person = FullPersonModel()
                        person.group_code = group.group_code
                        contents.add(person)
                    }
                    listener.onLoad(contents)
                }

                override fun onError(e: Throwable) {

                }

                override fun onComplete() {

                }
            })
    }

    override fun getAllMarks(listener: ILoadCallback<FullPersonModel>, string: String) {
        sMarkDao.getAllMarksByDiploma(string)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableMaybeObserver<List<FullMarks>>() {
                override fun onSuccess(list: List<FullMarks>) {
                    for (mark in list) {
                        var person = FullPersonModel()
                        person.mark_name.add(mark.mark_name)
                        contents.add(person)
                    }
                    listener.onLoad(contents)
                }

                override fun onError(e: Throwable) {

                }

                override fun onComplete() {

                }
            })
    }

    override fun getPersonsFromDB(listener: ILoadCallback<FullPersonModel>) {
        personDao.getAllPersons()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableMaybeObserver<List<FullPerson>>() {
                override fun onSuccess(list: List<FullPerson>) {
                    loop@ for (person in list) {
                        if (contents.isNotEmpty()) {
                            for (personFromContents in contents) {
                                if (personFromContents.person_id == person.person_id) {
                                    if (!personFromContents.payment_id.contains(person.payment_id)) {
                                        personFromContents.payment_date.add(person.payment_date)
                                        personFromContents.payment_sum.add(person.payment_sum)
                                        personFromContents.payment_id.add(person.payment_id)
                                    }
                                    if (!personFromContents.markId.contains(person.markId)) {
                                        personFromContents.markId.add(person.markId)
                                        personFromContents.mark_name.add(person.mark_name)
                                    }
                                    continue@loop
                                }
                            }
                            var fullPersonModel = FullPersonModel()
                            fullPersonModel.insertPerson(person)
                            contents.add(fullPersonModel)
                        } else {
                            var fullPersonModel = FullPersonModel()
                            fullPersonModel.insertPerson(person)
                            contents.add(fullPersonModel)
                        }
                    }
                    listener.onLoad(contents)
                }

                override fun onError(e: Throwable) {

                }

                override fun onComplete() {

                }
            })
    }

    override fun getPersonsFromDBByPayment(listener: ILoadCallback<FullPersonModel>, string: String) {
        personDao.getAllPersonsByPaymentDate(string)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableMaybeObserver<List<FullPerson>>() {
                override fun onSuccess(list: List<FullPerson>) {
                    loop@ for (person in list) {
                        if (contents.isNotEmpty()) {
                            for (personFromContents in contents) {
                                if (personFromContents.person_id == person.person_id) {
                                    if (!personFromContents.payment_id.contains(person.payment_id)) {
                                        personFromContents.payment_date.add(person.payment_date)
                                        personFromContents.payment_sum.add(person.payment_sum)
                                        personFromContents.payment_id.add(person.payment_id)
                                    }
                                    if (!personFromContents.markId.contains(person.markId)) {
                                        personFromContents.markId.add(person.markId)
                                        personFromContents.mark_name.add(person.mark_name)
                                    }
                                    continue@loop
                                }
                            }
                            var fullPersonModel = FullPersonModel()
                            fullPersonModel.insertPerson(person)
                            contents.add(fullPersonModel)
                        } else {
                            var fullPersonModel = FullPersonModel()
                            fullPersonModel.insertPerson(person)
                            contents.add(fullPersonModel)
                        }
                    }
                    listener.onLoad(contents)
                }

                override fun onError(e: Throwable) {

                }

                override fun onComplete() {

                }
            })
    }

    override fun getPersonsFromDBBySocial(listener: ILoadCallback<FullPersonModel>, string: String) {
        personDao.getAllPersonsBySocialName(string)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableMaybeObserver<List<FullPerson>>() {
                override fun onSuccess(list: List<FullPerson>) {
                    loop@ for (person in list) {
                        if (contents.isNotEmpty()) {
                            for (personFromContents in contents) {
                                if (personFromContents.person_id == person.person_id) {
                                    if (!personFromContents.payment_id.contains(person.payment_id)) {
                                        personFromContents.payment_date.add(person.payment_date)
                                        personFromContents.payment_sum.add(person.payment_sum)
                                        personFromContents.payment_id.add(person.payment_id)
                                    }
                                    if (!personFromContents.markId.contains(person.markId)) {
                                        personFromContents.markId.add(person.markId)
                                        personFromContents.mark_name.add(person.mark_name)
                                    }
                                    continue@loop
                                }
                            }
                            var fullPersonModel = FullPersonModel()
                            fullPersonModel.insertPerson(person)
                            contents.add(fullPersonModel)
                        } else {
                            var fullPersonModel = FullPersonModel()
                            fullPersonModel.insertPerson(person)
                            contents.add(fullPersonModel)
                        }
                    }
                    listener.onLoad(contents)
                }

                override fun onError(e: Throwable) {

                }

                override fun onComplete() {

                }
            })
    }
}