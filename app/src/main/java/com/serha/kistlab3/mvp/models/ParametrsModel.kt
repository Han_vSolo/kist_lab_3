package com.serha.kistlab3.mvp.models

import com.serha.kistlab3.App
import com.serha.kistlab3.db.daos.CafedraDao
import com.serha.kistlab3.db.daos.PaymentDao
import com.serha.kistlab3.db.daos.SDiplomaDao
import com.serha.kistlab3.db.daos.SSocialDao
import com.serha.kistlab3.db.entities.Cafedra
import com.serha.kistlab3.db.entities.Payment
import com.serha.kistlab3.db.entities.SDiploma
import com.serha.kistlab3.db.entities.SSocial
import com.serha.kistlab3.helpers.OnLoadComplete
import com.serha.kistlab3.mvp.contracts.ParametrsContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableMaybeObserver
import io.reactivex.schedulers.Schedulers

class ParametrsModel(
    val sSocialDao: SSocialDao,
    val paymentDao: PaymentDao,
    val cafedraDao: CafedraDao,
    val sDiplomaDao: SDiplomaDao
) : ParametrsContract.Model {

    var socialList = ArrayList<SSocial>()
    var paymentList = ArrayList<Payment>()
    var cafedralist = ArrayList<Cafedra>()
    var diplomaList = ArrayList<SDiploma>()

    override fun getAllSocials(listener: OnLoadComplete) {
        sSocialDao.getAllSocialNames()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableMaybeObserver<List<SSocial>>() {
                override fun onSuccess(list: List<SSocial>) {
                    socialList.addAll(list)
                    App.mInstance.listOfStrings.clear()
                    for (string in list) {
                        App.mInstance.listOfStrings.add(string.name)
                    }
                    listener.onLoadComplete()
                }

                override fun onError(e: Throwable) {

                }

                override fun onComplete() {

                }
            })
    }

    override fun getAllPayments(listener: OnLoadComplete) {
        paymentDao.getAllPayments()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableMaybeObserver<List<Payment>>() {
                override fun onSuccess(list: List<Payment>) {
//                    paymentList.addAll(list)
                    App.mInstance.listOfStrings.clear()
                    loop@
                    for (string in list) {
                        for (payment in paymentList) {
                            if (payment.paymentDate == string.paymentDate) {
                                continue@loop
                            }
                        }
                        App.mInstance.listOfStrings.add(string.paymentDate)
                        paymentList.add(string)
                    }
                    listener.onLoadComplete()
                }

                override fun onError(e: Throwable) {

                }

                override fun onComplete() {

                }
            })
    }

    override fun getAllCafedras(listener: OnLoadComplete) {
        cafedraDao.getAllCafedras()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableMaybeObserver<List<Cafedra>>() {
                override fun onSuccess(list: List<Cafedra>) {
                    cafedralist.addAll(list)
                    App.mInstance.listOfStrings.clear()
                    for (string in list) {
                        App.mInstance.listOfStrings.add(string.name)
                    }
                    listener.onLoadComplete()
                }

                override fun onError(e: Throwable) {

                }

                override fun onComplete() {

                }
            })
    }

    override fun getAllDiplomas(listener: OnLoadComplete) {
        sDiplomaDao.getAllDiplomas()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableMaybeObserver<List<SDiploma>>() {
                override fun onSuccess(list: List<SDiploma>) {
                    diplomaList.addAll(list)
                    App.mInstance.listOfStrings.clear()
                    for (string in list) {
                        App.mInstance.listOfStrings.add(string.name)
                    }
                    listener.onLoadComplete()
                }

                override fun onError(e: Throwable) {

                }

                override fun onComplete() {

                }
            })
    }
}