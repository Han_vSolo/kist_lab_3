package com.serha.kistlab3.mvp.presenters

import com.serha.kistlab3.App
import com.serha.kistlab3.R
import com.serha.kistlab3.mvp.base.BaseActivity
import com.serha.kistlab3.mvp.base.PresenterBase
import com.serha.kistlab3.mvp.contracts.ChooseContract
import com.serha.kistlab3.mvp.models.ChooseModel
import com.serha.kistlab3.mvp.views.fragments.ContentFragment
import com.serha.kistlab3.mvp.views.fragments.ParametrsFragment

class ChoosePresenter(val model: ChooseModel) : PresenterBase<ChooseContract.View>(), ChooseContract.Presenter {

    override fun onClick(i: Int, activity: BaseActivity) {
        if (App.mInstance.isOnline()) {
            if (i == 1) {
                val f = ParametrsFragment().getInstance(i)
                activity.showFragment(f)
            } else if (i == 2) {
                val f = ParametrsFragment().getInstance(i)
                activity.showFragment(f)
            } else if (i == 3) {
                val f = ParametrsFragment().getInstance(i)
                activity.showFragment(f)
            } else if (i == 4) {
                val f = ParametrsFragment().getInstance(i)
                activity.showFragment(f)
            } else if (i == 5) {
                activity.showFragment(ContentFragment())
            }
        } else {
            view?.showError(App.mInstance.resources.getString(R.string.no_internet_connection))
        }
    }

    override fun viewIsReady() {

    }
}