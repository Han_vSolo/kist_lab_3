package com.serha.kistlab3.mvp.presenters

import com.serha.kistlab3.FullPersonModel
import com.serha.kistlab3.mvp.base.ILoadCallback
import com.serha.kistlab3.mvp.base.PresenterBase
import com.serha.kistlab3.mvp.contracts.ContentContract
import com.serha.kistlab3.mvp.models.ContentModel

class ContentPresenter(val model: ContentModel) : PresenterBase<ContentContract.View>(), ContentContract.Presenter,
    ILoadCallback<FullPersonModel> {

    override fun getData(): ArrayList<FullPersonModel> {
        return model.contents
    }

    override fun getDataByParametrs(type: Int, string: String) {
        if (type == 1) {
            view?.showProgress()
            model.getPersonsFromDBBySocial(this, string)
        } else if (type == 2) {
            view?.showProgress()
            model.getPersonsFromDBByPayment(this, string)
        } else if (type == 3) {
            view?.showProgress()
            model.getAllGroups(this, string)
        } else if (type == 4) {
            view?.showProgress()
            model.getAllMarks(this, string)
        }
    }

    override fun viewIsReady() {
        view?.showProgress()
        model.getPersonsFromDB(this)
    }

    override fun onLoad(items: List<FullPersonModel>) {
        view?.hideProgress()
        view?.notifyDataSetChanged()
    }

    override fun onNoData() {
    }

    override fun onError(e: String) {
    }
}