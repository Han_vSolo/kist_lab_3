package com.serha.kistlab3.mvp.presenters

import com.serha.kistlab3.App
import com.serha.kistlab3.R
import com.serha.kistlab3.helpers.OnLoadComplete
import com.serha.kistlab3.mvp.base.BaseActivity
import com.serha.kistlab3.mvp.base.PresenterBase
import com.serha.kistlab3.mvp.contracts.ParametrsContract
import com.serha.kistlab3.mvp.models.ParametrsModel
import com.serha.kistlab3.mvp.views.fragments.ContentFragment

class ParametrsPresenter(val model: ParametrsModel) : PresenterBase<ParametrsContract.View>(),
    ParametrsContract.Presenter,
    OnLoadComplete {

    override fun getParametrs(type: Int) {
        view?.showProgress()
        when (type) {
            1 -> {
                model.getAllSocials(this)
            }
            2 -> {
                model.getAllPayments(this)
            }
            3 -> {
                model.getAllCafedras(this)
            }
            4 -> {
                model.getAllDiplomas(this)
            }
        }
    }

    override fun startQuery(type: Int, parametr: String, activity: BaseActivity) {
        if (App.mInstance.isOnline()) {
            val f = ContentFragment().getInstance(type, parametr)
            activity.showFragment(f)
        } else {
            view?.showError(App.mInstance.resources.getString(R.string.no_internet_connection))
        }
    }

    override fun onLoadComplete() {
        view?.hideProgress()
    }

    override fun viewIsReady() {

    }
}