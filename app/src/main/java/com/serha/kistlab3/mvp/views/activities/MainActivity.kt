package com.serha.kistlab3.mvp.views.activities

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import com.serha.kistlab3.R
import com.serha.kistlab3.db.entities.*
import com.serha.kistlab3.helpers.OnLoadComplete
import com.serha.kistlab3.helpers.RxRoomHelper
import com.serha.kistlab3.mvp.base.BaseActivity
import com.serha.kistlab3.mvp.views.fragments.ChooseFragment
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf


class MainActivity : BaseActivity(), OnLoadComplete {

    companion object {
        const val PREFERENCES_FIRST_START = "PREFERENCES_FIRST_START"
    }

    val prefs: SharedPreferences by inject { parametersOf(this) }
    val rxRoomHelper: RxRoomHelper by inject()
    var count = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        if (prefs.getBoolean(PREFERENCES_FIRST_START, true)) {
            prefs.edit().putBoolean(PREFERENCES_FIRST_START, false).apply()
            insertInfo()
        } else {
            progressBarDB.visibility = View.GONE
            showFirstFragment(ChooseFragment())
        }
    }

    fun insertInfo() {
        progressBarDB.visibility = View.VISIBLE
        val sprivelege1 = SPrivilege(0, "Ребенок АТОшника")
        val sprivelege2 = SPrivilege(0, "Ребенок беженца")
        val sprivelege3 = SPrivilege(0, "Ребенок олимпийского чемпиона")
        val sprivelege4 = SPrivilege(0, "Ребенок из многодетной семьи")
        val sprivelege5 = SPrivilege(0, "Ребенок шахтера")
        val sprivelege6 = SPrivilege(0, "Нет льготы")
        rxRoomHelper.insertSPrivilegeData(listOf(sprivelege1, sprivelege2, sprivelege3, sprivelege4, sprivelege5), this)
    }

    override fun onLoadComplete() {
        count++

        if (count == 1) {
            val sSocial1 = SSocial(0, "Ребенок военослужащего")
            val sSocial2 = SSocial(0, "Ребенок госслужащего")
            val sSocial3 = SSocial(0, "Ребенок рабочего")
            rxRoomHelper.insertSSocialData(listOf(sSocial1, sSocial2, sSocial3), this)
        }

        if (count == 2) {
            val sContractKind1 = SContractKind(0, "Контракт на 17000 грн")
            val sContractKind2 = SContractKind(0, "Контракт на 19000 грн")
            val sContractKind3 = SContractKind(0, "Контракт на 21000 грн")
            val sContractKind4 = SContractKind(0, "Контракт на 23000 грн")
            rxRoomHelper.insertSContractKindData(
                listOf(sContractKind1, sContractKind2, sContractKind3, sContractKind4),
                this
            )
        }

        if (count == 3) {
            val sFinance1 = SFinance(0, "Гривны")
            val sFinance2 = SFinance(0, "Доллары")
            val sFinance3 = SFinance(0, "Евро")
            rxRoomHelper.insertSFinanceData(listOf(sFinance1, sFinance2, sFinance3), this)
        }

        if (count == 4) {
            val cafedra1 = Cafedra(0, "Кафедра автоматизированных систем обработки информации и управления", "АСОИУ")
            val cafedra2 = Cafedra(0, "Кафедра вычислительной техники", "ВТ")
            val cafedra3 = Cafedra(0, "Кафедра автоматики и управления в технических системах", "АУТС")
            val cafedra4 = Cafedra(0, "Кафедра технической кибернетики", "ТК")
            rxRoomHelper.insertCafedraData(listOf(cafedra1, cafedra2, cafedra3, cafedra4), this)
        }

        if (count == 5) {
            rxRoomHelper.insertSDiplomaData(
                listOf(
                    SDiploma(0, "Підсистема аналізу результатів поточної атестації студентів"),
                    SDiploma(
                        0,
                        "Розширення функцій модуля авторизації (запам'ятати дані авторизації, сінхронізація з електронною поштою)"
                    ),
                    SDiploma(0, "Автоматизація процесу створення індивідуального навчального плану студента"),
                    SDiploma(0, "Підсистема \"Довідник дисциплін ВНЗ\""),
                    SDiploma(0, "Реалiзацiя системи розумного будинку з використанням хмарних технологiй"),
                    SDiploma(0, "Розробка веб-додатку для ведення туристичних пам'яток на основі архітектури REST"),
                    SDiploma(0, "Відлагоджувач програмного забезпечення"),
                    SDiploma(
                        0,
                        "Реалізація механізму поведінкової сегментації користувачей на сайті електронної коммерції"
                    ),
                    SDiploma(0, "Соціальна мережа"),
                    SDiploma(0, "Система планування виробництва"),
                    SDiploma(0, "Розробка веб-додатку для контролю ігредієнтів на складі ресторанного комлексу"),
                    SDiploma(0, "Розробка унікальної landingpage з анімацією. \"Вода\""),
                    SDiploma(0, "Написание сайта для мониторинга территорий online"),
                    SDiploma(0, "Розробка унікальної landingpage фірми по оздоблювальним роботам"),
                    SDiploma(0, "Разработка Телеграмм Бота для заказа билетов"),
                    SDiploma(0, "Сайт знайомств на Angular, Material і PHP"),
                    SDiploma(0, "Автоматизація процесу вибору дисциплін студентом"),
                    SDiploma(0, "Конвертація даних поточної відомості в Excel (php)"),
                    SDiploma(0, "Разработка Телеграмм Бота, как альтернативы сервиса по продаже авто"),
                    SDiploma(0, "Реализовать простой алгоритм отслеживания изменений на камере для охранных систем"),
                    SDiploma(
                        0,
                        "Створення мобільного додатку для розумного дому з використанням оптимізації семантичного аналізу"
                    ),
                    SDiploma(
                        0,
                        "Программа для управления полетом реактивного робототехнического зонда в арктических условиях"
                    ),
                    SDiploma(
                        0,
                        "Система інтелектуального аналізу характеристик об'єктів наземного моніторингу з використанням БПЛА"
                    ),
                    SDiploma(0, "Разработка Телеграмм Бота, как альтернативы интернет магазину"),
                    SDiploma(0, "Автоматизована система користування розкладом на мобільних додатках."),
                    SDiploma(0, "Сайт для дистанційного керування системою харчування домашніх тварин"),
                    SDiploma(0, "Система моніторингу та конфігурації компактної метеостанції"),
                    SDiploma(0, "Підсистема \"Довідник дисциплін ВНЗ\""),
                    SDiploma(0, "Веб-приложение для управления системой отопления умного дома"),
                    SDiploma(
                        0,
                        "Конструктор приложения оценки параметров объекта мониторинга для аграрной промышленности"
                    )
                ), this
            )
        }

        if (count == 6) {
            val sMark1 = SMark(0, "A")
            val sMark2 = SMark(0, "B")
            val sMark3 = SMark(0, "C")
            val sMark4 = SMark(0, "D")
            val sMark5 = SMark(0, "E")
            rxRoomHelper.insertSMarkData(listOf(sMark1, sMark2, sMark3, sMark4, sMark5), this)
        }

        if (count == 7) {
            val speciality1 =
                Speciality(0, 1, "Программное обеспечение информационных управляющих систем и технологий", "ПОИУСТ")
            val speciality2 = Speciality(0, 2, "Компьютерные системы и сети", "КСС")
            val speciality3 = Speciality(0, 3, "Информационные управляющие системы и технологии", "ИУСТ")
            val speciality4 =
                Speciality(0, 4, "Программное обеспечение интеллектуальных и робототехнических систем", "ПОИРС")
            val speciality5 = Speciality(0, 2, "Технологии программирования для компьютерных систем и сетей", "ТПКСС")
            val speciality6 = Speciality(0, 3, "Компьютеризированные системы управления", "КСУ")
            rxRoomHelper.insertSpecialityData(
                listOf(
                    speciality1,
                    speciality2,
                    speciality3,
                    speciality4,
                    speciality5,
                    speciality6
                ), this
            )
        }

        if (count == 8) {
            val group1 = Groups(0, 1, "ИК-43", "01.09.2014")
            val group2 = Groups(0, 2, "ИА-42", "01.09.2014")
            val group3 = Groups(0, 3, "ИТ-41", "01.09.2014")
            val group4 = Groups(0, 4, "ИХ-43", "01.09.2014")
            val group5 = Groups(0, 5, "ИВ-41", "01.09.2014")
            val group6 = Groups(0, 6, "ИН-42", "01.09.2014")
            val group7 = Groups(0, 1, "ИК-62", "01.09.2015")
            val group8 = Groups(0, 2, "ИК-52", "01.09.2016")
            rxRoomHelper.insertGroupsData(listOf(group1, group2, group3, group4, group5, group6, group7, group8), this)
        }

        if (count == 9) {
            rxRoomHelper.insertPersonData(
                listOf(
                    Person(
                        0,
                        "Песоцкий",
                        "Александер",
                        "Иванович",
                        "26.07.1998",
                        "Мужской",
                        "Киев",
                        "ул. Пушкина 9А",
                        "+380991231231",
                        1
                    ),
                    Person(
                        0,
                        "Фесенко",
                        "Вероника",
                        "Игоревна",
                        "13.11.1997",
                        "Женский",
                        "Павлоград",
                        "ул. Янгеля 10",
                        "+380993001229",
                        3
                    ),
                    Person(
                        0,
                        "Яценко",
                        "Андрей",
                        "Павлович",
                        "21.03.1998",
                        "Мужской",
                        "Полтава",
                        "ул. Горная 7",
                        "+380963019287",
                        2
                    ),
                    Person(
                        0,
                        "Осипян",
                        "Александр",
                        "Павлович",
                        "09.07.1998",
                        "Мужской",
                        "Ровно",
                        "ул. Франка 56",
                        "+380993018298",
                        1
                    ),
                    Person(
                        0,
                        "Байдала",
                        "Максим",
                        "Игоревич",
                        "11.11.1996",
                        "Мужской",
                        "Киев",
                        "ул. Горького 8",
                        "+380746123932",
                        2
                    ),
                    Person(
                        0,
                        "Хоменко",
                        "Ирина",
                        "Владимировна",
                        "13.11.1997",
                        "Женский",
                        "Киев",
                        "ул. Фанского 54",
                        "+380951023213",
                        1
                    ),
                    Person(
                        0,
                        "Качин",
                        "Мохамед",
                        "Петрович",
                        "07.03.1998",
                        "Мужской",
                        "Полтава",
                        "ул. Столетняя 32",
                        "+380964019233",
                        3
                    ),
                    Person(
                        0,
                        "Уткин",
                        "Дмитрий",
                        "Алексеевич",
                        "08.08.1996",
                        "Мужской",
                        "Львов",
                        "ул. Ленина 123",
                        "+380941232238",
                        2
                    ),
                    Person(
                        0,
                        "Соболев",
                        "Иван",
                        "Артасович",
                        "29.12.1997",
                        "Мужской",
                        "Коблево",
                        "ул. Пропанская 14",
                        "+380952343123",
                        1
                    ),
                    Person(
                        0,
                        "Виталий",
                        "Цаль",
                        "Артасович",
                        "12.12.1996",
                        "Мужской",
                        "Винница",
                        "ул. Солевая 13",
                        "+380951231232",
                        3
                    ),
                    Person(
                        0,
                        "Фонина",
                        "Алина",
                        "Ивановна",
                        "30.09.1996",
                        "Женский",
                        "Полтава",
                        "ул. Франкова 12",
                        "+380921239339",
                        1
                    ),
                    Person(
                        0,
                        "Афинина",
                        "Наталия",
                        "Владимировна",
                        "30.08.1997",
                        "Женский",
                        "ул. Колина 2",
                        "ул. Янгеля 199",
                        "+38092121239",
                        1
                    ),
                    Person(
                        0,
                        "Афонин",
                        "Родион",
                        "Петрович",
                        "16.09.1998",
                        "Мужской",
                        "Ивано-Франковск",
                        "ул. Артилерийская 3",
                        "+39081234523",
                        1
                    ),
                    Person(
                        0,
                        "Алехин",
                        "Павел",
                        "Александрович",
                        "03.03.1997",
                        "Мужской",
                        "Москва",
                        "ул. Ватутина 12",
                        "+380964239233",
                        3
                    ),
                    Person(
                        0,
                        "Аксонов",
                        "Максим",
                        "Максимови",
                        "13.09.1998",
                        "Мужской",
                        "Киев",
                        "ул. Янгеля 10",
                        "+380921211233",
                        2
                    ),
                    Person(
                        0,
                        "Болдин",
                        "Виктор",
                        "Александрович",
                        "13.08.1997",
                        "Мужской",
                        "Киев",
                        "ул. Янгеля 15",
                        "+38092121233",
                        3
                    ),
                    Person(
                        0,
                        "Финин",
                        "Андрей",
                        "Максимович",
                        "12.07.1996",
                        "Мужской",
                        "Киев",
                        "ул. Янгеля 11",
                        "+38092121231",
                        1
                    ),
                    Person(
                        0,
                        "Франклин",
                        "Виталий",
                        "Павлович",
                        "01.04.1997",
                        "Мужской",
                        "Киев",
                        "ул. Янгеля 119",
                        "+38092121554",
                        1
                    ),
                    Person(
                        0,
                        "Рощин",
                        "Евгений",
                        "Родионов",
                        "21.10.1997",
                        "Мужской",
                        "Киев",
                        "ул. Янгеля 19",
                        "+38092127676",
                        3
                    ),
                    Person(
                        0,
                        "Максимюк",
                        "Карина",
                        "Дмитриевна",
                        "31.01.1997",
                        "Женский",
                        "Киев",
                        "ул. Янгеля 06",
                        "+38092129898",
                        3
                    ),
                    Person(
                        0,
                        "Ростова",
                        "Галина",
                        "Викторона",
                        "03.03.1996",
                        "Женский",
                        "Киев",
                        "ул. Янгеля 107",
                        "+38092121456",
                        3
                    ),
                    Person(
                        0,
                        "Галинина",
                        "Евгения",
                        "Ивановна",
                        "10.10.1997",
                        "Женский",
                        "Киев",
                        "ул. Янгеля 07",
                        "+38092121123",
                        1
                    ),
                    Person(
                        0,
                        "Фонин",
                        "Алексей",
                        "Виталиевич",
                        "11.11.1998",
                        "Мужской",
                        "Киев",
                        "ул. Янгеля 7",
                        "+38092121234",
                        2
                    ),
                    Person(
                        0,
                        "Растов",
                        "Николай",
                        "Викторович",
                        "03.03.1998",
                        "Мужской",
                        "Киев",
                        "ул. Янгеля 4",
                        "+38092121234",
                        2
                    ),
                    Person(
                        0,
                        "Жемчин",
                        "Ростислав",
                        "Максимович",
                        "12.03.1997",
                        "Мужской",
                        "Киев",
                        "ул. Янгеля 3",
                        "+38092121678",
                        1
                    ),
                    Person(
                        0,
                        "Ященко",
                        "Максим",
                        "Алексеев",
                        "13.04.1996",
                        "Мужской",
                        "Киев",
                        "ул. Янгеля 13",
                        "+38092125647",
                        2
                    ),
                    Person(
                        0,
                        "Физров",
                        "Николай",
                        "Павлович",
                        "14.05.1997",
                        "Мужской",
                        "Киев",
                        "ул. Янгеля 114",
                        "+38092128765",
                        1
                    ),
                    Person(
                        0,
                        "Андеров",
                        "Виктор",
                        "Петрович",
                        "15.12.1997",
                        "Мужской",
                        "Киев",
                        "ул. Янгеля 43",
                        "+38092121298",
                        3
                    ),
                    Person(
                        0,
                        "Кемич",
                        "Андрей",
                        "Игоревич",
                        "15.11.1996",
                        "Мужской",
                        "Киев",
                        "ул. Янгеля 6",
                        "+38092121245",
                        1
                    ),
                    Person(
                        0,
                        "Химич",
                        "Алексей",
                        "Павлович",
                        "12.02.1997",
                        "Мужской",
                        "Киев",
                        "ул. Янгеля 8",
                        "+38092121239",
                        2
                    )
                ), this
            )
        }

        if (count == 10) {
            rxRoomHelper.insertPersonPrivilegeData(
                listOf(
                    PersonPrivilege(0, 1, 1, "01.09.2014", "01.09.2016", ""),
                    PersonPrivilege(0, 2, 2, "01.09.2014", "01.09.2017", ""),
                    PersonPrivilege(0, 3, 3, "01.09.2014", "01.09.2018", ""),
                    PersonPrivilege(0, 4, 4, "01.09.2014", "01.09.2019", ""),
                    PersonPrivilege(0, 5, 2, "01.09.2014", "01.09.2017", ""),
                    PersonPrivilege(0, 6, 3, "01.09.2014", "01.09.2018", ""),
                    PersonPrivilege(0, 7, 4, "01.09.2014", "01.09.2017", ""),
                    PersonPrivilege(0, 8, 4, "01.09.2014", "01.09.2016", ""),
                    PersonPrivilege(0, 9, 3, "01.09.2014", "01.09.2018", ""),
                    PersonPrivilege(0, 10, 2, "01.09.2014", "01.09.2019", ""),
                    PersonPrivilege(0, 11, 2, "01.09.2014", "01.09.2018", ""),
                    PersonPrivilege(0, 12, 1, "01.09.2014", "01.09.2017", ""),
                    PersonPrivilege(0, 13, 1, "01.09.2014", "01.09.2018", ""),
                    PersonPrivilege(0, 14, 3, "01.09.2014", "01.09.2017", ""),
                    PersonPrivilege(0, 15, 4, "01.09.2014", "01.09.2017", ""),
                    PersonPrivilege(0, 16, 2, "01.09.2014", "01.09.2016", ""),
                    PersonPrivilege(0, 17, 1, "01.09.2014", "01.09.2016", ""),
                    PersonPrivilege(0, 18, 2, "01.09.2014", "01.09.2018", ""),
                    PersonPrivilege(0, 19, 3, "01.09.2014", "01.09.2019", ""),
                    PersonPrivilege(0, 20, 3, "01.09.2014", "01.09.2017", ""),
                    PersonPrivilege(0, 21, 4, "01.09.2014", "01.09.2017", ""),
                    PersonPrivilege(0, 22, 2, "01.09.2014", "01.09.2018", ""),
                    PersonPrivilege(0, 23, 1, "01.09.2014", "01.09.2019", ""),
                    PersonPrivilege(0, 24, 2, "01.09.2014", "01.09.2017", ""),
                    PersonPrivilege(0, 25, 1, "01.09.2014", "01.09.2016", ""),
                    PersonPrivilege(0, 26, 2, "01.09.2014", "01.09.2018", ""),
                    PersonPrivilege(0, 27, 3, "01.09.2014", "01.09.2016", ""),
                    PersonPrivilege(0, 28, 4, "01.09.2014", "01.09.2016", ""),
                    PersonPrivilege(0, 29, 3, "01.09.2014", "01.09.2017", ""),
                    PersonPrivilege(0, 30, 1, "01.09.2014", "01.09.2018", "")
                ), this
            )
        }

        if (count == 11) {
            rxRoomHelper.insertStudentData(
                listOf(
                    Student(0, 1, 1, 1, 1, ""),
                    Student(0, 2, 2, 2, 3, ""),
                    Student(0, 3, 3, 3, 3, ""),
                    Student(0, 4, 3, 4, 4, ""),
                    Student(0, 5, 2, 5, 5, ""),
                    Student(0, 6, 2, 6, 6, ""),
                    Student(0, 7, 1, 7, 7, ""),
                    Student(0, 8, 1, 8, 8, ""),
                    Student(0, 9, 2, 9, 9, ""),
                    Student(0, 10, 2, 10, 10, ""),
                    Student(0, 11, 2, 11, 11, ""),
                    Student(0, 12, 1, 12, 12, ""),
                    Student(0, 13, 1, 13, 13, ""),
                    Student(0, 14, 1, 14, 14, ""),
                    Student(0, 15, 1, 15, 15, ""),
                    Student(0, 16, 1, 16, 16, ""),
                    Student(0, 17, 1, 17, 17, ""),
                    Student(0, 18, 2, 18, 18, ""),
                    Student(0, 19, 1, 19, 19, ""),
                    Student(0, 20, 2, 20, 20, ""),
                    Student(0, 21, 3, 21, 21, ""),
                    Student(0, 22, 1, 22, 22, ""),
                    Student(0, 23, 3, 23, 23, ""),
                    Student(0, 24, 1, 24, 24, ""),
                    Student(0, 25, 2, 25, 25, ""),
                    Student(0, 26, 1, 26, 26, ""),
                    Student(0, 27, 1, 27, 27, ""),
                    Student(0, 28, 1, 28, 28, ""),
                    Student(0, 29, 1, 29, 29, ""),
                    Student(0, 30, 1, 30, 30, "")
                ), this
            )
        }

        if (count == 12) {
            rxRoomHelper.insertStudentGroupData(
                listOf(
                    StudentGroup(0, 1, 1, "01.09.2015"),
                    StudentGroup(0, 2, 2, "01.09.2015"),
                    StudentGroup(0, 3, 3, "01.09.2015"),
                    StudentGroup(0, 4, 4, "01.09.2015"),
                    StudentGroup(0, 5, 5, "01.09.2015"),
                    StudentGroup(0, 6, 6, "01.09.2015"),
                    StudentGroup(0, 7, 7, "01.09.2015"),
                    StudentGroup(0, 8, 8, "01.09.2015"),
                    StudentGroup(0, 9, 1, "01.09.2015"),
                    StudentGroup(0, 10, 2, "01.09.2015"),
                    StudentGroup(0, 11, 3, "01.09.2015"),
                    StudentGroup(0, 12, 4, "01.09.2015"),
                    StudentGroup(0, 13, 5, "01.09.2015"),
                    StudentGroup(0, 14, 6, "01.09.2015"),
                    StudentGroup(0, 15, 7, "01.09.2015"),
                    StudentGroup(0, 16, 8, "01.09.2015"),
                    StudentGroup(0, 17, 1, "01.09.2015"),
                    StudentGroup(0, 18, 2, "01.09.2015"),
                    StudentGroup(0, 19, 3, "01.09.2015"),
                    StudentGroup(0, 20, 4, "01.09.2015"),
                    StudentGroup(0, 21, 5, "01.09.2015"),
                    StudentGroup(0, 22, 6, "01.09.2015"),
                    StudentGroup(0, 23, 7, "01.09.2015"),
                    StudentGroup(0, 24, 8, "01.09.2015"),
                    StudentGroup(0, 25, 1, "01.09.2015"),
                    StudentGroup(0, 26, 2, "01.09.2015"),
                    StudentGroup(0, 27, 3, "01.09.2015"),
                    StudentGroup(0, 28, 4, "01.09.2015"),
                    StudentGroup(0, 29, 5, "01.09.2015"),
                    StudentGroup(0, 30, 6, "01.09.2015")
                ), this
            )
        }

        if (count == 13) {
            rxRoomHelper.insertContractData(
                listOf(
                    Contract(0, 1, 1, "01.09.2015", 1, 17000, ""),
                    Contract(0, 2, 2, "01.09.2015", 2, 19000, ""),
                    Contract(0, 3, 3, "01.09.2015", 3, 21000, ""),
                    Contract(0, 4, 4, "01.09.2015", 4, 23000, ""),
                    Contract(0, 5, 1, "01.09.2015", 5, 17000, ""),
                    Contract(0, 6, 2, "01.09.2015", 6, 19000, ""),
                    Contract(0, 7, 3, "01.09.2015", 7, 21000, ""),
                    Contract(0, 8, 4, "01.09.2015", 8, 23000, ""),
                    Contract(0, 9, 1, "01.09.2015", 9, 17000, ""),
                    Contract(0, 10, 2, "01.09.2015", 10, 19000, ""),
                    Contract(0, 11, 3, "01.09.2015", 11, 21000, ""),
                    Contract(0, 12, 4, "01.09.2015", 12, 23000, ""),
                    Contract(0, 13, 1, "01.09.2015", 13, 17000, ""),
                    Contract(0, 14, 2, "01.09.2015", 14, 19000, ""),
                    Contract(0, 15, 3, "01.09.2015", 15, 21000, ""),
                    Contract(0, 16, 4, "01.09.2015", 16, 23000, ""),
                    Contract(0, 17, 1, "01.09.2015", 17, 17000, ""),
                    Contract(0, 18, 2, "01.09.2015", 18, 19000, ""),
                    Contract(0, 19, 3, "01.09.2015", 19, 21000, ""),
                    Contract(0, 20, 4, "01.09.2015", 20, 23000, ""),
                    Contract(0, 21, 1, "01.09.2015", 21, 17000, ""),
                    Contract(0, 22, 2, "01.09.2015", 22, 19000, ""),
                    Contract(0, 23, 3, "01.09.2015", 23, 21000, ""),
                    Contract(0, 24, 4, "01.09.2015", 24, 23000, ""),
                    Contract(0, 25, 1, "01.09.2015", 25, 17000, ""),
                    Contract(0, 26, 2, "01.09.2015", 26, 19000, ""),
                    Contract(0, 27, 3, "01.09.2015", 27, 21000, ""),
                    Contract(0, 28, 4, "01.09.2015", 28, 23000, ""),
                    Contract(0, 29, 1, "01.09.2015", 29, 17000, ""),
                    Contract(0, 30, 2, "01.09.2015", 30, 19000, "")
                ), this
            )
        }

        if (count == 14) {
            rxRoomHelper.insertStudentMarksData(
                listOf(
                    StudentMarks(0, 1, 1),
                    StudentMarks(0, 1, 2),
                    StudentMarks(0, 1, 3),
                    StudentMarks(0, 2, 1),
                    StudentMarks(0, 2, 1),
                    StudentMarks(0, 2, 1),
                    StudentMarks(0, 3, 3),
                    StudentMarks(0, 3, 3),
                    StudentMarks(0, 3, 4),
                    StudentMarks(0, 4, 1),
                    StudentMarks(0, 4, 2),
                    StudentMarks(0, 4, 1),
                    StudentMarks(0, 5, 4),
                    StudentMarks(0, 5, 5),
                    StudentMarks(0, 5, 3),
                    StudentMarks(0, 6, 5),
                    StudentMarks(0, 6, 1),
                    StudentMarks(0, 6, 4),
                    StudentMarks(0, 7, 1),
                    StudentMarks(0, 7, 5),
                    StudentMarks(0, 7, 1),
                    StudentMarks(0, 8, 1),
                    StudentMarks(0, 8, 3),
                    StudentMarks(0, 8, 4),
                    StudentMarks(0, 9, 1),
                    StudentMarks(0, 9, 2),
                    StudentMarks(0, 9, 3),
                    StudentMarks(0, 10, 1),
                    StudentMarks(0, 10, 5),
                    StudentMarks(0, 10, 4),
                    StudentMarks(0, 11, 3),
                    StudentMarks(0, 11, 1),
                    StudentMarks(0, 11, 2),
                    StudentMarks(0, 12, 3),
                    StudentMarks(0, 12, 1),
                    StudentMarks(0, 12, 3),
                    StudentMarks(0, 13, 1),
                    StudentMarks(0, 13, 4),
                    StudentMarks(0, 13, 1),
                    StudentMarks(0, 14, 5),
                    StudentMarks(0, 14, 1),
                    StudentMarks(0, 14, 3),
                    StudentMarks(0, 15, 1),
                    StudentMarks(0, 15, 2),
                    StudentMarks(0, 15, 1),
                    StudentMarks(0, 16, 3),
                    StudentMarks(0, 16, 1),
                    StudentMarks(0, 16, 2),
                    StudentMarks(0, 17, 1),
                    StudentMarks(0, 17, 1),
                    StudentMarks(0, 17, 3),
                    StudentMarks(0, 18, 1),
                    StudentMarks(0, 18, 4),
                    StudentMarks(0, 18, 5),
                    StudentMarks(0, 19, 5),
                    StudentMarks(0, 19, 5),
                    StudentMarks(0, 19, 2),
                    StudentMarks(0, 20, 1),
                    StudentMarks(0, 20, 4),
                    StudentMarks(0, 20, 3),
                    StudentMarks(0, 21, 1),
                    StudentMarks(0, 21, 2),
                    StudentMarks(0, 21, 1),
                    StudentMarks(0, 22, 3),
                    StudentMarks(0, 22, 3),
                    StudentMarks(0, 22, 4),
                    StudentMarks(0, 23, 5),
                    StudentMarks(0, 23, 1),
                    StudentMarks(0, 23, 1),
                    StudentMarks(0, 24, 1),
                    StudentMarks(0, 24, 5),
                    StudentMarks(0, 24, 5),
                    StudentMarks(0, 25, 5),
                    StudentMarks(0, 25, 1),
                    StudentMarks(0, 25, 2),
                    StudentMarks(0, 26, 3),
                    StudentMarks(0, 26, 4),
                    StudentMarks(0, 26, 2),
                    StudentMarks(0, 27, 1),
                    StudentMarks(0, 27, 2),
                    StudentMarks(0, 27, 1),
//                    StudentMarks(0, 27, 1),
                    StudentMarks(0, 28, 1),
                    StudentMarks(0, 28, 3),
                    StudentMarks(0, 28, 3),
                    StudentMarks(0, 29, 3),
                    StudentMarks(0, 29, 1),
                    StudentMarks(0, 29, 4),
                    StudentMarks(0, 30, 4),
                    StudentMarks(0, 30, 1),
                    StudentMarks(0, 30, 5)
                ), this
            )
        }

        if (count == 15) {
            rxRoomHelper.insertPaymentData(
                listOf(
                    Payment(0, 1, "01.08.2015", 17000, 1),
                    Payment(0, 1, "01.08.2016", 17000, 2),
                    Payment(0, 2, "01.08.2015", 19000, 3),
                    Payment(0, 2, "01.08.2016", 19000, 4),
                    Payment(0, 3, "01.08.2015", 21000, 5),
                    Payment(0, 3, "01.08.2016", 21000, 6),
                    Payment(0, 4, "01.08.2015", 23000, 7),
                    Payment(0, 4, "01.08.2016", 23000, 8),
                    Payment(0, 5, "01.08.2015", 17000, 9),
                    Payment(0, 5, "01.08.2016", 17000, 10),
                    Payment(0, 6, "01.08.2015", 19000, 11),
                    Payment(0, 6, "01.08.2016", 19000, 12),
                    Payment(0, 7, "01.08.2015", 21000, 13),
                    Payment(0, 7, "01.08.2016", 21000, 14),
                    Payment(0, 8, "01.08.2015", 23000, 15),
                    Payment(0, 8, "01.08.2016", 23000, 16),
                    Payment(0, 9, "01.08.2015", 17000, 17),
                    Payment(0, 9, "01.08.2016", 17000, 18),
                    Payment(0, 10, "01.08.2015", 19000, 19),
                    Payment(0, 10, "01.08.2016", 19000, 20),
                    Payment(0, 11, "01.08.2015", 21000, 21),
                    Payment(0, 11, "01.08.2016", 21000, 22),
                    Payment(0, 12, "01.08.2015", 23000, 23),
                    Payment(0, 12, "01.08.2016", 23000, 24),
                    Payment(0, 13, "01.08.2015", 17000, 25),
                    Payment(0, 13, "01.08.2016", 17000, 26),
                    Payment(0, 14, "01.08.2015", 19000, 27),
                    Payment(0, 14, "01.08.2016", 19000, 28),
                    Payment(0, 15, "01.08.2015", 21000, 29),
                    Payment(0, 15, "01.08.2016", 21000, 30),
                    Payment(0, 16, "02.08.2015", 23000, 31),
                    Payment(0, 16, "02.08.2016", 23000, 32),
                    Payment(0, 17, "02.08.2015", 17000, 33),
                    Payment(0, 17, "02.08.2016", 17000, 34),
                    Payment(0, 18, "02.08.2015", 19000, 35),
                    Payment(0, 18, "02.08.2016", 19000, 36),
                    Payment(0, 19, "02.08.2015", 21000, 37),
                    Payment(0, 19, "02.08.2016", 21000, 38),
                    Payment(0, 20, "02.08.2015", 23000, 39),
                    Payment(0, 20, "02.08.2016", 23000, 40),
                    Payment(0, 21, "02.08.2015", 17000, 41),
                    Payment(0, 21, "02.08.2016", 17000, 42),
                    Payment(0, 22, "02.08.2015", 19000, 43),
                    Payment(0, 22, "02.08.2016", 19000, 44),
                    Payment(0, 23, "02.08.2015", 21000, 45),
                    Payment(0, 23, "02.08.2016", 21000, 46),
                    Payment(0, 24, "02.08.2015", 23000, 47),
                    Payment(0, 24, "02.08.2016", 23000, 48),
                    Payment(0, 25, "02.08.2015", 17000, 49),
                    Payment(0, 25, "02.08.2016", 17000, 50),
                    Payment(0, 26, "02.08.2015", 19000, 51),
                    Payment(0, 26, "02.08.2016", 19000, 52),
                    Payment(0, 27, "02.08.2015", 21000, 53),
                    Payment(0, 27, "02.08.2016", 21000, 54),
                    Payment(0, 28, "02.08.2015", 23000, 55),
                    Payment(0, 28, "02.08.2016", 23000, 56),
                    Payment(0, 29, "02.08.2015", 17000, 57),
                    Payment(0, 29, "02.08.2016", 17000, 58),
                    Payment(0, 30, "02.08.2015", 19000, 59),
                    Payment(0, 30, "02.08.2016", 19000, 60)
                ), this
            )
        }

        if (count == 16) {
            progressBarDB.visibility = View.GONE
            showFirstFragment(ChooseFragment())
        }
    }

    override fun getContainerID(): Int {
        return R.id.vContainer
    }
}
