package com.serha.kistlab3.mvp.views.fragments

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.serha.kistlab3.R
import com.serha.kistlab3.mvp.base.BaseActivity
import com.serha.kistlab3.mvp.base.BaseFragment
import com.serha.kistlab3.mvp.contracts.ChooseContract
import com.serha.kistlab3.mvp.presenters.ChoosePresenter
import kotlinx.android.synthetic.main.fragment_choose.*
import org.koin.android.ext.android.inject
import org.koin.android.scope.ext.android.bindScope
import org.koin.android.scope.ext.android.getOrCreateScope

class ChooseFragment : BaseFragment(), ChooseContract.View {

    val presenter: ChoosePresenter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindScope(getOrCreateScope("chooseSession"))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_choose, container, false)

        (activity as AppCompatActivity).supportActionBar!!.setTitle(R.string.app_name)
        (activity as AppCompatActivity).supportActionBar!!.subtitle = null

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setBackNavigation(false)

        presenter.attachView(this)

        firstQuery.setOnClickListener {
            presenter.onClick(1, activity as BaseActivity)
        }
        secondQuery.setOnClickListener {
            presenter.onClick(2, activity as BaseActivity)
        }
        thirdQuery.setOnClickListener {
            presenter.onClick(3, activity as BaseActivity)
        }
        fourthQuery.setOnClickListener {
            presenter.onClick(4, activity as BaseActivity)
        }
        fifthQuery.setOnClickListener {
            presenter.onClick(5, activity as BaseActivity)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun onBackPressed(): Boolean {
        return false
    }
}