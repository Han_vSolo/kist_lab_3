package com.serha.kistlab3.mvp.views.fragments

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.serha.kistlab3.App

class ChoseDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity!!)

        builder.setTitle("Выберите параметр:")
            .setNegativeButton("Отмена", null)
            .setItems(
                App.mInstance.getArray()
            ) { dialog, which ->
                val i = Intent()
                i.putExtra(SELECTED, App.mInstance.listOfStrings[which])
                targetFragment!!.onActivityResult(targetRequestCode, Activity.RESULT_OK, i)
            }

        return builder.create()
    }

    companion object {
        val CHANGE_DIALOG_TAG = "CHANGE_DIALOG_TAG"
        val SELECTED = "SELECTED "
        val CHANGE_DIALOG_ID = 5
    }
}