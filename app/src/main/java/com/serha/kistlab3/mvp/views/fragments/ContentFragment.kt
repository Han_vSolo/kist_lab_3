package com.serha.kistlab3.mvp.views.fragments

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.serha.kistlab3.R
import com.serha.kistlab3.adapters.ContentAdapter
import com.serha.kistlab3.mvp.base.BaseActivity
import com.serha.kistlab3.mvp.base.BaseFragment
import com.serha.kistlab3.mvp.contracts.ContentContract
import com.serha.kistlab3.mvp.presenters.ContentPresenter
import com.serha.kistlab3.views.VerticalSpaceItemDecoration
import kotlinx.android.synthetic.main.fragment_content.*
import org.koin.android.ext.android.inject
import org.koin.android.scope.ext.android.bindScope
import org.koin.android.scope.ext.android.getOrCreateScope

class ContentFragment : BaseFragment(), ContentContract.View {

    companion object {
        const val CONTENT_TYPE = "CONTENT_TYPE"
        const val PARAMETR_STRING = "PARAMETR_STRING"
    }

    val presenter: ContentPresenter by inject()
    lateinit var adapter: ContentAdapter //by inject { parametersOf(presenter.getData(), activity as BaseActivity) }
    var type = 5
    var stringQuery = ""

    fun getInstance(type: Int, parametrString: String): ContentFragment {
        val f = ContentFragment()
        val bundle = Bundle()
        bundle.putInt(CONTENT_TYPE, type)
        bundle.putString(PARAMETR_STRING, parametrString)
        f.arguments = bundle
        return f
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindScope(getOrCreateScope("contentSession"))
        if (arguments != null) {
            type = arguments!!.getInt(CONTENT_TYPE, 5)
            stringQuery = arguments!!.getString(PARAMETR_STRING, "")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_content, container, false)

        (activity as AppCompatActivity).supportActionBar!!.title = "Query"
        (activity as AppCompatActivity).supportActionBar!!.subtitle = null

        setBackNavigation(true)

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)

        val mLayoutManager = LinearLayoutManager(context)
        recyclerView!!.layoutManager = mLayoutManager
        adapter = ContentAdapter(presenter.getData(), activity as BaseActivity, type)
        recyclerView!!.adapter = adapter
        recyclerView!!.addItemDecoration(VerticalSpaceItemDecoration(24))
    }

    override fun onResume() {
        super.onResume()
        if (type == 5) {
            presenter.viewIsReady()
        } else {
            presenter.getDataByParametrs(type, stringQuery)
        }
    }

    override fun onBackPressed(): Boolean {
        return false
    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
    }

    override fun notifyDataSetChanged() {
        adapter.notifyDataSetChanged()
    }
}