package com.serha.kistlab3.mvp.views.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.serha.kistlab3.R
import com.serha.kistlab3.mvp.base.BaseActivity
import com.serha.kistlab3.mvp.base.BaseFragment
import com.serha.kistlab3.mvp.contracts.ParametrsContract
import com.serha.kistlab3.mvp.presenters.ParametrsPresenter
import kotlinx.android.synthetic.main.fragment_parametrs.*
import org.koin.android.ext.android.inject
import org.koin.android.scope.ext.android.bindScope
import org.koin.android.scope.ext.android.getOrCreateScope

class ParametrsFragment : BaseFragment(), ParametrsContract.View {

    var type = 1
    private var filledListener: View.OnTouchListener? = null
    val presenter: ParametrsPresenter by inject()

    fun getInstance(type: Int): ParametrsFragment {
        val f = ParametrsFragment()
        val bundle = Bundle()
        bundle.putInt(ContentFragment.CONTENT_TYPE, type)
        f.arguments = bundle
        return f
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindScope(getOrCreateScope("parametrSession"))
        if (arguments != null) {
            type = arguments!!.getInt(ContentFragment.CONTENT_TYPE, 1)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_parametrs, container, false)

        (activity as AppCompatActivity).supportActionBar!!.title = "Query"
        (activity as AppCompatActivity).supportActionBar!!.subtitle = null

        setBackNavigation(true)

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        filledListener = View.OnTouchListener { view, motionEvent ->
            if (MotionEvent.ACTION_UP == motionEvent.action) {
                val choseCountryDialog = ChoseDialogFragment()
                choseCountryDialog.setTargetFragment(
                    this@ParametrsFragment,
                    ChoseDialogFragment.CHANGE_DIALOG_ID
                )
                choseCountryDialog.show(
                    activity?.supportFragmentManager?.beginTransaction(),
                    ChoseDialogFragment.CHANGE_DIALOG_TAG
                )
            }
            true
        }

        query.setOnClickListener {
            presenter.startQuery(type, parametr.text.toString(), activity as BaseActivity)
        }

        parametr.setOnTouchListener(filledListener)

        presenter.attachView(this)
        presenter.getParametrs(type)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun onBackPressed(): Boolean {
        return false
    }

    override fun showProgress() {
        parametr.visibility = View.INVISIBLE
        query.visibility = View.INVISIBLE
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        parametr.visibility = View.VISIBLE
        query.visibility = View.VISIBLE
        progressBar.visibility = View.INVISIBLE
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (isAdded && !isRemoving) {
            super.onActivityResult(requestCode, resultCode, data)
            when (requestCode) {
                ChoseDialogFragment.CHANGE_DIALOG_ID -> if (resultCode == Activity.RESULT_OK) {
                    val selected = data.getStringExtra(ChoseDialogFragment.SELECTED)
                    parametr.setText(selected)
                }
            }
        }
    }
}