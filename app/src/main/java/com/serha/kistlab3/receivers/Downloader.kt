package com.serha.kistlab3.receivers

import android.content.Context
import com.serha.kistlab3.App
import java.io.*

class Downloader {
    companion object {

        fun getCacheDir(context: Context?): File? {
            if (context == null) {
                return null
            }

            val directory = context.getExternalFilesDir(null)
            val hiddenDir = File(directory, ".cache")
            if (!hiddenDir.exists()) {
                hiddenDir.mkdir()
            }
            return hiddenDir
        }

        fun writeFileAsString(filePath: String, data: String) {
            val file = File(getCacheDir(App.mInstance), filePath)

            val dir = File(file.parentFile.absolutePath)
            if (!dir.exists()) {
                dir.mkdir()
            }

            if (file.exists()) {
                file.delete()
            }
            try {
                file.createNewFile()

                val buf = BufferedWriter(FileWriter(file, true))
                buf.append(data)
                buf.newLine()
                buf.close()

            } catch (e: IOException) {
                e.printStackTrace()
            }

        }

        @Throws(java.io.IOException::class)
        fun readFileAsString(filePath: String): String {
            val fileData = StringBuffer(1000)
            val reader = BufferedReader(FileReader(getCacheDir(App.mInstance)!!.toString() + filePath))
            var buf = CharArray(1024)
            var numRead = reader.read(buf)
            while (numRead != -1) {
                val readData = String(buf, 0, numRead)
                fileData.append(readData)
                buf = CharArray(1024)
                numRead = reader.read(buf)
            }
            reader.close()
            return fileData.toString()
        }
    }
}