package com.serha.kistlab3.views

import android.app.ProgressDialog
import android.content.Context
import android.view.Window
import com.serha.kistlab3.R

object LoadingView {

    private var dialog: ProgressDialog? = null

    val isNull: Boolean
        get() = dialog == null

    fun show(context: Context) {
        if (dialog == null) {
            dialog = ProgressDialog(context)
            dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog!!.setMessage(context.getString(R.string.loading))
            dialog!!.setCancelable(false)
            dialog!!.show()
        }
    }

    fun hide() {
        if (dialog != null) {
            try {
                dialog!!.dismiss()
            } catch (e: IllegalArgumentException) {
                //CustomCrossFadeSlidingPaneLayout.lang.IllegalArgumentException: View=DecorView@d8caf31[] not attached to window manager
            }

            dialog = null
        }
    }
}